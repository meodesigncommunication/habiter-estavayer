<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

define('DB_NAME', 'estavayer');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost');

if (!defined('IS_PRODUCTION')) {
	define('IS_PRODUCTION', false);
}

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données.
  * N'y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+Q;|@.on|:bDqW-R~9Rlb)|+-~7<|6tL+J!fxNI|5JZ6U69HtC4I 3QgNGp%]7)[');
define('SECURE_AUTH_KEY',  '|5FsZPQBF(0GZ.x&dBoZWRR{b%WUm;Q5-y>t/N6;zU$`7H|7>Bkw<Hsk`a6,V:Kb');
define('LOGGED_IN_KEY',    'p],&4nsYIB`-zkuch}$4c,aQ$R-w[Qnj}A>Dzq$Dx/;/7+-.[q`-_]|ou{2uJMb>');
define('NONCE_KEY',        's-PSac_|S)Q|c,)1i;^IY5,M!a-AKhxK+wS*+4^WOp3m+FSDM-Zu scGed0t-$5B');
define('AUTH_SALT',        'hi.2L/`k|hyD&D6(@es{!Y1R#uW.te#SKuMfM-o)S>mB sCkB@j{-[*. |suhiTV');
define('SECURE_AUTH_SALT', 'ng@7)grxhgU.l7`B#93FBi+Lz2X79X&-]CX~Bs)A1sO{SBJz(+&=XAy WZH#K~`Y');
define('LOGGED_IN_SALT',   '}-|4?Bm-IGrxr)D>Q+5ItTfdS/vWgUq~D}1Gr07:J.KY9Kns:~7)!vK`)GDz*#pl');
define('NONCE_SALT',       'ev1Wh%W/+A8&KcaccrFq:jNa5_hepO%gR k8h)Y`8:VZ:O9_?KDAq/A,yo)#Ld~u');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode deboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 */
define('WP_DEBUG', false);

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');