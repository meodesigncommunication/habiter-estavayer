<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 * 
 * Template Name: Landing Page
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

define('HOME_PAGE_ID', 42);

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5shiv.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>
	<link href='http://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyfifteen' ); ?></a>

	<div id="page-header">
		<?php /* <a class="estavayer-login" href="<?php echo get_permalink(HOME_PAGE_ID); ?>">login</a><div class="clear"></div> */ ?>
		<header id="masthead" class="site-header" role="banner">
			<div id="site-logo"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/les-portes-du-lac-logo@2x.png" alt="Les Portes Du Lac" /></div>
			<div class="site-branding">
				<button class="secondary-toggle"><?php _e( 'Menu and widgets', 'twentyfifteen' ); ?></button>
			</div><!-- .site-branding -->
		</header><!-- .site-header -->

		<?php get_sidebar(); ?>
	</div><!-- #page-header -->

	<div id="content" class="site-content">
		<div id="photos-banner"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/bg-photo-banner@2x.png" alt="Habiter Estavayer" title="Cr&eacute;dit photo centre: Patrick Nouhailler" /></div>

		<div id="primary" class="content-area landing-page-content">
			<main id="main" class="site-main" role="main">

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
					</div><!-- .entry-content -->

				</article><!-- #post-## -->

				<div class="landing-side-call-action">
					<div class="c2a-container">
					<?php
					$site_url = get_bloginfo( 'url' );
					echo do_shortcode('[call2actionbtn title="M\'AVERTIR LORS DE LA<br/><b>COMMERCIALISATION</b>" url="'.$site_url.'/contact/" target="" linkclass="fancybox-iframe"]'); ?>
					</div>
				</div>
				<div class="landing-button-sub-text">Soyez-les premiers &agrave; acqu&eacute;rir votre logement en vous inscrivant au moyen du bouton &laquo;M'avertir lors de la commercialisation&raquo;.</div>
				<?php

			// End the loop.
			endwhile;
			?>

			</main><!-- .site-main -->
		</div><!-- .content-area -->

	</div><!-- .site-content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<div class="footer-logo"><a href="http://www.gefiswiss.ch/" target="_blank"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/gefiswiss-sa-logo-footer@2x.png" alt="GeFIswiss SA" /></a></div>
			<div class="footer-address">Avenue Mon-Repos 24 - 1005 Lausanne<br/>021 613 80 70</div>
		</div><!-- .site-info -->
	</footer><!-- .site-footer -->
	<div class="super-footer"></div>
</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>