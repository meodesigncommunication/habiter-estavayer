<?php
/**
 * Template Name: Page Grid C2
 */
global $post,
       $mk_options;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );


if ( empty( $page_layout ) ) {
    $page_layout = 'full';
}
$padding = ($padding == 'true') ? 'no-padding' : '';

//$grid_elements = get_field('grid_elements', $post->ID);
//$content_elements = get_field('content_elements', $post->ID);

get_header(); ?>
    <div id="theme-page">
        <?php /* Call To Action [ call_to_action_content | call_to_action_url | call_to_action_enabled ] */
        $active_call = gefi_get_active_call2action($post->ID);

        if($active_call) {
            ?>
            <div id="call-to-action">
                <div class="c2a-container">
                    <div class="c2a-content"><?php echo __($active_call['content']); ?></div>
                    <?php if(!empty($active_call['url'])){ ?><div class="c2a-link"><a href="<?php echo $active_call['url']; ?>"><?php echo __($active_call['url_text']); ?></a></div><?php } ?>
                </div>
            </div>
            <?php
        }
        /* End Call To Action */ ?>

        <div class="mk-main-wrapper-holder">
            <div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper <?php echo $page_layout; ?>-layout <?php echo $padding; ?> mk-grid vc_row-fluid">
                <div class="theme-content <?php echo $padding; ?>" itemprop="mainContentOfPage">
                    <?php
                    $info_txt = get_field( "info_texte", $post->ID );
                    $info_color = get_field( "info_color", $post->ID );
                    if(!empty($info_txt)):
                        ?>
                        <a href="http://habiter-estavayer.ch/nous-trouver/" title="Contact"><div id="banner-info-home" style="background-color: <?php echo $info_color; ?>;"><p style="letter-spacing: 1px;color:#fff;text-align: center;font-weight: bold; text-transform: uppercase"><?php echo $info_txt; ?></p></div></a>
                    <?php endif; ?>
                    <?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
                        <?php the_content();?>
                        <div class="clearboth"></div>
                        <?php wp_link_pages( 'before=<div id="mk-page-links">'.__( 'Pages:', 'mk_framework' ).'&after=</div>' ); ?>
                    <?php endwhile; ?>
                </div>
                <?php
                if(isset($mk_options['pages_comments']) && $mk_options['pages_comments'] == 'true') {
                    comments_template( '', true );
                }
                ?>
                <?php if ( $page_layout != 'full' ) get_sidebar(); ?>
                <div class="clearboth"></div>
            </div>
            <div class="clearboth"></div>
        </div>
    </div>
<?php get_footer(); ?>