<?php
/**
 * Single Lot Template
 */

define('USE_SVGS', false);


// Extract image details, and its fallback
function mred_lot_get_images($primary_image, $fallback_image) {
	$result_primary = get_stylesheet_directory_uri() . '/images/transparent.gif';
	$result_fallback = $result_primary;

	// If SVGs are disabled, and primary image is SVG, use the fallback as the primary
	if (is_array($primary_image) && preg_match('/svg/', $primary_image['mime_type']) && !USE_SVGS) {
		$primary_image = $fallback_image;
	}

	if ($primary_image && $primary_image['url']) {
		$result_primary = $primary_image['url'];
	}

	if ($fallback_image && $fallback_image['url']) {
		$result_fallback = $fallback_image['url'];
	}
	else {
		$result_fallback = preg_replace("/\.svg$/", ".png", $result_primary);
	}

	return array($result_primary, $result_fallback);
}


$padding = get_post_meta( $post->ID, '_padding', true );

$padding = ($padding == 'true') ? 'no-padding' : '';

wp_enqueue_style('js_composer_front');

mred_show_page_header();

$lot = mred_get_lot($post->ID);
$floor = mred_get_floor($lot['floor_id']);
$building = mred_get_building($floor['building_id']);
$plan = mred_get_plan($lot['plan_id']);
$entry = mred_get_entry($lot['entry_id']);

$availability_class = mred_get_availability_class($lot['availability']);
$availability = mred_get_availability_description($lot['availability']);

// Update : Print price f user can and price is filled
if(isset($_SESSION['pricesEnabled']) && $_SESSION['pricesEnabled'] === true && $lot['availability'] == 'available'){
	if(isset($lot['price']) && $lot['price'] != '') {
		$availabiltyPrice = number_format((int)$lot['price'], 0, ".", "'");
		$availability = $availabiltyPrice.'CHF';
	}
}

$pdf_instructions = get_field('pdf_instructions', 'option');

$breadcrumb = mred_get_lot_breadcrumb($lot['id']);

list( $plan_image,           $plan_image_fallback           ) = mred_lot_get_images($plan['detailed_plan_image'], $plan['detailed_plan_image_fallback']);
list( $floor_position_image, $floor_position_image_fallback ) = mred_lot_get_images($lot['floor_position_image'], $lot['floor_position_image_fallback']);
list( $lot_situation_image,  $lot_situation_image_fallback  ) = mred_lot_get_images($lot['lot_situation_image'],  $lot['lot_situation_image_fallback'] );

$additional_images = array();
if (!empty($plan['additional_images'])) {
	foreach($plan['additional_images'] as $additional_image) {
		$additional_images[] = mred_lot_get_images($additional_image['additional_image'],  $additional_image['additional_image_fallback'] );
	}
}

$separator = do_shortcode('[vc_separator color="grey" align="align_center" el_width="50"]');

$floor_names = array();
if(is_array($lot)){
	foreach ($lot['all_floors'] as $loop_floor_id => $loop_floor_details) {
		$loop_floor = mred_get_floor($loop_floor_id);
		$floor_names[] = '<span class="floor_name">' . $loop_floor['ordinal'] . '</span>';
	}
}

list($code_sector, $code_building, $code_floor, $code_lot) = explode('.', $lot['code']);

$code_sector = substr($code_sector,1);

echo mred_get_lot_subheader($lot['id']);

?>
<div id="theme-page">
	<div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper <?php echo $padding; ?> mk-grid vc_row-fluid">
		<div class="theme-content <?php echo $padding; ?>" itemprop="mainContentOfPage">

			<?php while ( have_posts() ) : the_post(); ?>


				<div class="wpb_row  vc_row-fluid  mk-fullwidth-false add-padding-0 attched-false">
					<div class="vc_span12 wpb_column column_container single-lot-header">
						<?php echo $breadcrumb; ?>
						<div class="lot_summary">
							<h1><span class="lot_name">&Eacute;tape <?php echo $code_sector ?> - <?php echo $lot['name']; ?><?php if ($lot['multifloor']) { ?><span class="duplex"> | duplex</span><?php } ?></span></h1>
							<h2>
                                <span class="building_name"><?php echo $building['name']; ?></span> | <span class="rooms"><?php echo $lot['pieces']; ?> <?php _e('p', MREDTEMPLATES_TEXT_DOMAIN); ?> | <?php echo join(" - ", $floor_names) ?> | </span><span class="availability <?php echo $availability_class; ?>"><?php echo $availability; ?></span>
                                <br/>
                                <span class="availability_date"><?php echo get_field('availability_date', $building['id']) ; ?></span>
                            </h2>
						</div>

						<?php if (!empty($lot['pdf'])) { ?>
							<div class="pdf-download">
								<?php echo mred_get_download_button($lot['id']); ?>
							</div>
						<?php } ?>

					</div>
				</div>

				<div class="extra-line l-top"><div class="inner-line"></div></div>

				<div class="wpb_row  vc_row-fluid  mk-fullwidth-false add-padding-0 attched-false">
					<div class="main-image-wrapper vc_span6 wpb_column column_container ">
						<img class="main_plan" src="<?php echo $plan_image; ?>" data-fallback="<?php echo $plan_image_fallback; ?>">
					</div>
				</div>

				<?php if (!empty($additional_images)) {
					foreach ($additional_images as $additional_image) { ?>
						<div class="extra-line"><div class="inner-line"></div></div>
						<div class="wpb_row  vc_row-fluid  mk-fullwidth-false add-padding-0 attched-false">
							<div class="additional-image-wrapper vc_span6 wpb_column column_container ">
								<img class="additional-image" src="<?php echo $additional_image[0]; ?>" data-fallback="<?php echo $additional_image[1]; ?>">
							</div>
						</div><?php
					}
				} ?>

				<div class="extra-line"><div class="inner-line"></div></div>

				<div class="wpb_row vc_row  vc_row-fluid  mk-fullwidth-true  attched-false vc_row-fluid">
				    <div style="" class="vc_col-sm-4 wpb_column column_container ">
						<div class="lot_details">
							<h4><?php _e('Surface', MREDTEMPLATES_TEXT_DOMAIN); ?></h4>
							<?php // echo $separator;
							$lot_veranda = get_field('surface_veranda', $lot['id']);
							$lot_parking = get_field('parking', $lot['id']);
							?>
							<ul class="areas">
								<?php if (!empty($lot['surface_interior'])) { ?><li class="interior"><span class="description"><?php _e('Apartment', MREDTEMPLATES_TEXT_DOMAIN); ?></span>: <span class="figure"><?php echo $lot['surface_interior']; ?> m<sup>2</sup></span></li><?php } ?>
								<?php if (!empty($lot['surface_terrace'])) { ?><li class="terrace"><span class="description"><?php _e('Terrace area', MREDTEMPLATES_TEXT_DOMAIN); ?></span>: <span class="figure"><?php echo $lot['surface_terrace']; ?> m<sup>2</sup></span></li><?php } ?>
								<?php if (!empty($lot['surface_balcony'])) { ?><li class="balcony"><span class="description"><?php _e('Balcony area', MREDTEMPLATES_TEXT_DOMAIN); ?></span>: <span class="figure"><?php echo $lot['surface_balcony']; ?> m<sup>2</sup></span></li><?php } ?>
								<?php if (!empty($lot['surface_garden'])) { ?><li class="gardens"><span class="description"><?php _e('Garden area', MREDTEMPLATES_TEXT_DOMAIN); ?></span>: <span class="figure"><?php echo $lot['surface_garden']; ?> m<sup>2</sup></span></li><?php } ?>
								<?php if (!empty($lot_veranda)) { ?><li class="veranda"><span class="description"><?php _e('Surface Veranda', MREDTEMPLATES_TEXT_DOMAIN); ?></span>: <span class="figure"><?php echo $lot_veranda; ?> m<sup>2</sup></span></li><?php } ?>
								<?php if (!empty($lot_parking)) { ?><li class="parking"><span class="description"><?php _e('Parking', MREDTEMPLATES_TEXT_DOMAIN); ?></span>: <span class="figure"><?php echo $lot_parking; ?></span></li><?php } ?>
								<li class="weighted"><span class="description"><?php _e('Surface area', MREDTEMPLATES_TEXT_DOMAIN); ?></span>: <span class="figure"><?php echo $lot['surface_weighted']; ?> m<sup>2</sup></span></li>
							</ul>
						</div>
				    </div>
				    <div style="" class="vc_col-sm-4 wpb_column column_container ">
						<?php if ($floor_position_image) { ?>
							<div class="lot_floor">
								<h4><?php _e('Floor', MREDTEMPLATES_TEXT_DOMAIN); ?></h4>
								<?php // echo $separator; ?>
								<div class="floor_position_wrapper">
									<img src="<?php echo $floor_position_image; ?>" data-fallback="<?php echo $floor_position_image_fallback; ?>" alt="<?php _e('Floor', MREDTEMPLATES_TEXT_DOMAIN); ?>" />
								</div>
							</div>
						<?php } ?>
				    </div>
				    <div style="" class="vc_col-sm-4 wpb_column column_container ">
						<?php if ($lot_situation_image) { ?>
							<div class="lot_situation">
								<h4><?php _e('Situation', MREDTEMPLATES_TEXT_DOMAIN); ?></h4>
								<?php // echo $separator; ?>
								<div class="floor-plan-wrapper">
									<img src="<?php echo $lot_situation_image; ?>" data-fallback="<?php echo $lot_situation_image_fallback; ?>" alt="<?php _e('Situation', MREDTEMPLATES_TEXT_DOMAIN); ?>" />
								</div>
							</div>
						<?php } ?>

				    </div>
				</div>
				<?php mred_get_projectview_row(); ?>
				<div class="clearboth"></div>
			<?php endwhile; // end of the loop ?>
		</div><!-- .theme-content -->
		<div class="clearboth"></div>
	</div><!-- theme-page-wrapper -->
	<div class="clearboth"></div>
</div><!-- #theme-page -->

<?php get_footer(); ?>
