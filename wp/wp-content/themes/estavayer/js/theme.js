(function($){

	function make_imagemaps_responsive() {
		$('img[usemap]').rwdImageMaps();
	}

	function add_floor_png_over_functionality() {
		var isMobile = navigator.userAgent.match(/iPhone|iPad|iPod|Android|BlackBerry|IEMobile/i) ? true : false; // not perfect, but will get the important cases

		if (isMobile) {
			return;
		}

		$('.single-development .image-wrapper area').each(function() {
			$(this).on('mouseover', function() {
			  var overlay_class = $(this).data('over'),
				  $overlay = $('#overlay_' + overlay_class),
				  $label = $('#label_overlay_' + overlay_class);
			  $overlay.attr('src', $overlay.data('over'));
			  $label.show();
			});

			$(this).on('mouseout', function() {
			  var overlay_class = $(this).data('over'),
				  $overlay = $('#overlay_' + overlay_class),
				  $label = $('#label_overlay_' + overlay_class);

			  $overlay.attr('src', $overlay.data('off'));
			  $label.hide();
			});
		});
	}

	// Document ready function
	$(function() {
		make_imagemaps_responsive();
		add_floor_png_over_functionality();
	});

})(jQuery);


jQuery(document).ready(function() {

	/* Home page logo fade in */
	if(jQuery('.logo-estavayer-home-footer img').length){
		// jQuery( ".logo-estavayer-home-footer img" ).fadeIn( 10000 );
	}

	var onResize = function() {
		jQuery('#mk-responsive-wrap').css('max-height', 'none');
		jQuery('#mk-responsive-wrap').css('overflow-y', 'auto');
		jQuery('#mk-responsive-nav').css('max-height', 'none');

		if(jQuery('.mk-edge-slider.mk-swiper-container').length){
			var heightToApply = jQuery( window ).height();

			if (jQuery('.mk-resposnive-logo').is(":visible")) {
				heightToApply -= jQuery('.mk-header-inner').height();
			}

			jQuery('.mk-edge-slider.mk-swiper-container').css('height', heightToApply);
			jQuery('.edge-slider-holder.mk-swiper-wrapper').css('height', heightToApply);
			jQuery('.swiper-slide').css('height', heightToApply);
			jQuery('.mk-section-image.slide').css('height', heightToApply);
		}

		if (jQuery('.swiper-pagination').length) {
			if (jQuery( window ).height() > 500) {
				jQuery('.swiper-pagination').show();
			}
			else {
				jQuery('.swiper-pagination').hide();
			}
		}
	};
	jQuery(window).resize(onResize);
	jQuery(window).on('orientationchange', onResize);

});

jQuery(window).load(function() {
	// Do this on load (not document ready) so the logo visibility check should work consistently
	jQuery(window).trigger('resize');
});
