<aside id="mk-sidebar" class="mk-builtin">
    <div class="sidebar-wrapper">
    <?php 
    global $post;
    
    if ( is_home() || is_category() || is_single() ) {?>
   		<div class="actualites-sidebar">
   		<?php dynamic_sidebar( __('Archive Widget Area','mk_framework') ); ?>
   		</div>
   		<?php 
   	}
   	else {
   	
	    if(isset($post)){
	
	    	mk_sidebar_generator( 'get_sidebar', $post->ID);
	
	    }else{
	
	    	mk_sidebar_generator( 'get_sidebar', false);
	
	    } 
   	}
   	?>
    </div>
</aside>