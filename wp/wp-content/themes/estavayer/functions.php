<?php

/* Init */
	/* Add Sessions */
	function meo_init_session() {
		if (!session_id()) session_start();

		// Print Pricelist (Not enabled by default)
		if(!isset($_SESSION['pricesEnabled'])) $_SESSION['pricesEnabled'] = false;
	}
	add_action('init', 'meo_init_session', 1);

	add_image_size( 'homepage-grid-square', 230, 230, true );

	/* Add Shortcode capability to widgets */
	add_filter('widget_text', 'do_shortcode');


/* Parent Theme */
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

	function theme_enqueue_styles() {
		wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	}

	add_action('wp_enqueue_scripts', 'theme_enqueue_print_styles', 9999);
	function theme_enqueue_print_styles() {
		wp_enqueue_style( 'estavayer-menu', get_stylesheet_directory_uri() . '/css/estavayer-menu.css', array(), filemtime( get_stylesheet_directory() . '/css/estavayer-menu.css'), 'screen' );
		wp_enqueue_style( 'print-styles',   get_stylesheet_directory_uri() . '/css/print.css',          array(), filemtime( get_stylesheet_directory() . '/css/print.css'),          'print'  );
	}
	# Dequeue vendors script to modify it and implement the photoswipe library
	add_action( 'wp_enqueue_scripts', 'gefiswiss_enqueue_scripts', 11 );
	function gefiswiss_enqueue_scripts() {
		wp_dequeue_script( 'theme-scripts-min' );
		wp_dequeue_script( 'theme-scripts' );
		global $mk_options;
		$theme_data = wp_get_theme("Jupiter");
		
		// Mobile quick php level for chrome android/desktop, js issues with functions
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		
		// same test in footer.php
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
		{
			if($mk_options['minify-js'] == 'true') {
				wp_enqueue_script( 'gefiswiss-theme-scripts-min', get_stylesheet_directory_uri() .'/js/min/scripts-vendors-mobile-ck.js', array( 'jquery' ), $theme_data['Version'], true );
			} else {
				wp_enqueue_script( 'gefiswiss-theme-scripts', get_stylesheet_directory_uri() .'/js/scripts-vendors-mobile.js', array( 'jquery' ), $theme_data['Version'], true );
			}
		}
		else {
			if($mk_options['minify-js'] == 'true') {
				wp_enqueue_script( 'gefiswiss-theme-scripts-min', get_stylesheet_directory_uri() .'/js/min/scripts-vendors-ck.js', array( 'jquery' ), $theme_data['Version'], true );
			} else {
				wp_enqueue_script( 'gefiswiss-theme-scripts', get_stylesheet_directory_uri() .'/js/scripts-vendors.js', array( 'jquery' ), $theme_data['Version'], true );
			}
		}
	}
	

###################
/* Smart Capture */
###################

	define('MREDTEMPLATES_TEXT_DOMAIN', 'mred-templates');
	define('PLAN_PAGE_SLUG', 'plans-par-liste');

/* Load translations */
	add_action('init', 'mredtemplate_lang');

	function mredtemplate_lang() {
		load_theme_textdomain(MREDTEMPLATES_TEXT_DOMAIN, get_stylesheet_directory() . '/mredt-languages');
	}

/* Load custom javascript */
	add_action( 'wp_enqueue_scripts', 'mredtemplate_enqueue_scripts_and_styles', 11);
	function mredtemplate_enqueue_scripts_and_styles() {
		wp_register_script('responseivemagemaps', get_stylesheet_directory_uri() . '/js/jquery.rwdImageMaps.js', array('jquery'), 'v1.5');
		wp_register_script('mred-theme', get_stylesheet_directory_uri() . '/js/theme.js', array('responseivemagemaps'), filemtime( get_stylesheet_directory() . '/js/theme.js'));

		wp_enqueue_script('responseivemagemaps');
		wp_enqueue_script('mred-theme');
	}

/* Add filter menu state to body */
	add_filter('body_class', 'gefi_body_classes');
	function gefi_body_classes($classes) {
	        $classes[] = 'filter-menu-closed';
	        return $classes;
	}

/* Allow use of shortcodes in CF7 forms */
	add_filter( 'wpcf7_form_elements', 'do_shortcode' );

/* Allow custom action depending on arguments */
	define('ENCODED_PRICE_LIST_ID', 22069); // = 1298 (see MeoScCf7Integration code) * 17 + 3 (see MeoScCf7Utilities::encodeAttachmentId)
	define('ENCODED_PRICE_LIST_ID_C1', 199158); // = 11715 (see MeoScCf7Integration code) * 17 + 3 (see MeoScCf7Utilities::encodeAttachmentId)
	define('ENCODED_PRICE_LIST_ID_C2', 199175); // = 11716 (see MeoScCf7Integration code) * 17 + 3 (see MeoScCf7Utilities::encodeAttachmentId)

	add_filter( 'wpcf7_ajax_json_echo', 'gefi_form_ajax', 10, 2);
	function gefi_form_ajax($items, $result) {
		$list_page_id =  mred_get_page_id_from_slug( PLAN_PAGE_SLUG );
		$list_page_link = get_permalink($list_page_id);
		if (($_GET['file_id'] == ENCODED_PRICE_LIST_ID || $_GET['file_id'] == ENCODED_PRICE_LIST_ID_C1 || $_GET['file_id'] == ENCODED_PRICE_LIST_ID_C2) && $result['status'] == 'mail_sent' && !empty($list_page_link)) {
			$items['message'] .= '<br>Nous vous redirigeons maintenant à la liste des prix.';
			if (!array_key_exists('onSentOk', $items)) {
				$items['onSentOk'] = array();
			}
			$items['onSentOk'][] = 'setTimeout(function () { window.top.location.href = "' . $list_page_link . '"; }, 2500); ';
		}

		return $items;
	}



###################
/* Smart Capture */
###################


/* Override default MRED functions */
	function mred_get_plan_filter_classes($lot_id) {
		$lot = mred_get_lot($lot_id);
		$floor = mred_get_floor($lot['floor_id']);
		$building = mred_get_building($floor['building_id']);
		$sector = mred_get_sector($building['sector_id']);
		$plan = mred_get_plan($lot['plan_id']);
		
		# Etape
		$buildingavailability = get_field('step_building', $building['id']);

		$filter_classes = array(
			'building_' . $building['id'],
			'sector_' . $sector['id'],
			'rooms_' . preg_replace('/\D/', '_', $lot['pieces']),
			'entry_' . $lot['entry_id'],
			'lot_type_' . $lot['type']['slug'],
			'balcony_' . ( empty($lot['surface_balcony']) ? '0' : '1' ),
			'availability_' . mred_get_availability_class($lot['availability']),
			'buildingstep_' . filter_var($buildingavailability, FILTER_SANITIZE_NUMBER_INT)
		);

		foreach ($lot['all_floors'] as $floor_id => $floor_details) {
			$floor = mred_get_floor($floor_id);
			$filter_classes[] = 'floor_' . strip_tags(str_replace(' ', '_', $floor['ordinal']));
		}

		return $filter_classes;
	}

	// Plan filters.
	// Options can be added and removed with the plan_list_filter_options filter,
	// rather than overwriting the entire function
	function mred_get_plan_list_filters() {
		$buildings = mred_get_buildings();
		$sectors = mred_get_sectors();
		$floors = mred_get_floors();
		$lots = mred_get_lots();
		$lot_types = mred_get_lot_types();
		$entries = mred_get_entries();
		
		
		$building_options = array();
		$buildingstep_options = array();
		foreach ($buildings as $building) {
		    if(!get_field('building_full', $building['id'])) {
			    $building_options[$building['id']] = $building['code'];
            }
            $buildingavailability = get_field('step_building', $building['id']);
            
            if(!in_array($buildingavailability, $buildingstep_options)){
            	$buildingstep_options[filter_var($buildingavailability, FILTER_SANITIZE_NUMBER_INT)] = $buildingavailability;
            }
		}

		$sector_options = array();
		foreach ($sectors as $sector) {
			$sector_options[$sector['id']] = $sector['code'];
		}

		$floor_options = array();
		foreach ($floors as $floor) {
			// Selection per floor per buildling
			// $floor_options[$floor['id']] = $floor['ordinal'] . (count($buildings) > 1 ? ' - ' . $buildings[$floor['building_id']]['name'] : '');
			// Selection per floor type (rez, 1st, 2nd etc.)
			$floor_code = strip_tags(str_replace(' ', '_', $floor['ordinal']));
			$floor_options[$floor_code] = $floor['ordinal'];
		}

		$entry_options = array();
		foreach ($entries as $entry) {
			$entry_options[$entry['id']] = $entry['name'];
		}

		$lot_type_options = array();
		foreach ($lot_types as $lot_type) {
			$lot_type_options[$lot_type['slug']] = $lot_type['name'];
		}

		$room_options = array();
		foreach ($lots as $lot) {
			$escaped_rooms = preg_replace('/\D/', '_', $lot['pieces']);
			$room_options[$escaped_rooms] = $lot['pieces'] . ' ' . mred_translate('p.');
		}
		asort($room_options);
		
		$filter_menu_options = array(
			'all' => array(
				'name' => mred_translate('All'),
				'filter_type' => 'all',
				'classes' => 'chosen'
			),
			'lot_types' => array(
				'name' => mred_translate('Type'),
				'filter_type' => 'lot_type',
				'options' => $lot_type_options
			),
			'sector' => array(
				'name' => mred_translate('sector'),
				'filter_type' => 'sector',
				'options' => $sector_options
			),
			'building' => array(
				'name' => mred_translate('Building'),
				'filter_type' => 'building',
				'options' => $building_options
			),
			'floor' => array(
				'name' => mred_translate('Floor'),
				'filter_type' => 'floor',
				'options' => $floor_options
			),
			'entry' => array(
				'name' => mred_translate('Entry'),
				'filter_type' => 'entry',
				'options' => $entry_options
			),
			'rooms' => array(
				'name' => mred_translate('Rooms'),
				'filter_type' => 'rooms',
				'options' => $room_options
			),
			'balcony' => array(
				'name' => mred_translate('Balcony'),
				'filter_type' => 'balcony',
				'options' => array(
					'0' => mred_translate('No'),
					'1' => mred_translate('Yes')
				)
			),
			'availability' => array(
				'name' => mred_translate('Availability'),
				'filter_type' => 'availability',
				'options' => mred_get_availability_descriptions()
			),
			'buildingstep' => array(
				'name' => mred_translate('&Eacute;tape'),
				'filter_type' => 'buildingstep',
				'options' => $buildingstep_options
			)
		);

		$filter_menu_options = apply_filters('plan_list_filter_options', $filter_menu_options);


		$result = '';

		$result .= '<ul class="filter-menu sf-menu">';

		foreach ($filter_menu_options as $filter_menu_option) {
			$name = $filter_menu_option['name'];
			$filter_type = $filter_menu_option['filter_type'];
			$classes = empty($filter_menu_option['classes']) ? '' : $filter_menu_option['classes'];
			$options = $filter_menu_option['options'];
			$has_children = !empty($options);
			if ($has_children) {
				$classes .= ' has-children';
			}

			$result .= '<li' . ( $classes ? ' class="' . $classes . '"' : '' ) . '>';
			if (!$has_children) {
				$result .= '<a data-filter-type="' . $filter_type . '" href="#' . $filter_type . '">';
			}
			$result .= '<span class="filter-menu-header">' . $name . '</span>';
			if ($has_children) {

				$result .= '<ul class="submenu">';
				foreach ($options as $option_value => $option_name) {
                    if($filter_type == 'building') {
                        foreach ($buildings as $building) {
                            if($option_value == $building['id']) {
                                list($code_sector,$code_building) = explode('.',$building['code']);
                                $option_name = '&Eacute;tape '.substr($code_sector, 1).' - '.$option_name;
                            }
                        }
                    }
					$result .= '<li><a data-filter-type="' . $filter_type . '" data-filter-value="' . $option_value . '" href="#' . $filter_type . '_' . $option_value . '">' . $option_name . '</a></li>';
				}
				$result .= '</ul>';
			}

			if (!$has_children) {
				$result .= '</a>';
			}
			$result .= '</li>';

		}

		$result .= '</ul>'; // .filter-menu

		return $result;
	}

	function mred_get_lot_plan_classes() {
		return array('vc_col-sm-3', 'wpb_column', 'column_container');
	}

	function mred_get_lot_markup_list ($lot, $floor_link, $classes = array()) {

		global $download_lot_id;
		$download_lot_id = $lot['id'];

		$floor = mred_get_floor($lot['floor_id']);
		$building = mred_get_building($floor['building_id']);
		$lot_type = empty($lot['type']) ? '' : $lot['type']['slug'];
		$lot_link = get_permalink($lot['id']);
		if (function_exists('qtrans_convertURL')) {
			$lot_link = qtrans_convertURL($lot_link, $current_language);
		}
		$filter_classes = mred_get_plan_filter_classes($lot['id']);

		ob_start();

		// Availability / Price
		$availabiltyPrice = mred_get_availability_description($lot['availability']);

		if(isset($_SESSION['pricesEnabled']) && $_SESSION['pricesEnabled'] === true && $lot['availability'] == 'available'){
			if(isset($lot['price']) && $lot['price'] != '') {
				$availabiltyPrice = number_format((int)$lot['price'], 0, ".", "'");
				$availabiltyPrice = $availabiltyPrice.'CHF';
			}
		}

		$floor_links = array();
		foreach ($lot['all_floors'] as $loop_floor_id => $loop_floor_details) {
			$loop_floor = mred_get_floor($loop_floor_id);
			$floor_links[] = '<a href="' . $floor_link . '#floor_' . $loop_floor_id . '"><span class="list_building_text">' . $loop_floor['ordinal'] . '</span></a>';
		}

        list($code_sector, $code_building, $code_floor, $code_lot) = explode('.', $lot['code']);
        $code_sector = '&Eacute;tape '.substr($code_sector,1);


		?>

		<li class="apartment-list-row <?php echo join(" ", $classes); ?> <?php echo join(" ", mred_get_lot_list_classes()); ?> <?php echo join(" ", $filter_classes); ?>">
			<ul>
				<li class="apartment_code_and_availability">
					<a href="<?php echo $lot_link; ?>">
						<div class="apartment_code"><?php echo $code_sector.' - '.$lot['name']; ?><?php if ($lot['multifloor']) { ?><span class="duplex"> | duplex</span><?php } ?></div>
						<div class="availability"><?php echo $availabiltyPrice; ?></div>
					</a>
				</li>
				<li class="building"><a href="<?php echo $floor_link; ?>#floor_<?php echo $lot['floor_id']; ?>"><span class="list_icon list_icon_building"></span><span class="list_building_text"><?php echo $building['name']; ?></span></a></li>
				<li class="floor"><span class="list_icon list_icon_floor"></span><?php echo join(' - ', $floor_links); ?></li>
				<li class="rooms"><a href="<?php echo $lot_link; ?>"><span class="list_icon list_icon_rooms"></span><span class="list_building_text"><?php echo $lot['pieces']; echo '&nbsp;<span class="lower">' . mred_translate('p.') . '</span>'; ?></span></a></li>
				<li class="area"><a href="<?php echo $lot_link; ?>"><span class="list_icon list_icon_area"></span><span class="list_building_text"><?php echo $lot['surface_weighted']; echo '&nbsp;<span class="lower">m</span><sup>2</sup>';  ?></span></a></li>
				<?php if (!empty($lot['pdf'])) { ?>
					<li class="pdf-download"><a class="fancybox-iframe pdf-download-link" href="<?php echo mred_get_pdf_request_url($lot['id']); ?>"><?php echo mred_translate('Info pack'); ?></a></li>
				<?php } else { ?>
					<li class="pdf-download"><a class="see-lot-link" href="<?php echo $lot_link; ?>"><?php echo mred_translate('View lot details'); ?></a></li>
				<?php } ?>
			</ul>
		</li><?php

		return apply_filters( 'mred_get_lot_markup_list', ob_get_clean() );
	}

	// Return a list item containing the HTML for plan portion of the list page
	// Theme-specific classes (eg column classes) can be passed
	if (!function_exists( 'mred_get_lot_markup_plan')) {
		function mred_get_lot_markup_plan ($lot, $floor_link, $classes = array()) {
			$floor = mred_get_floor($lot['floor_id']);
			$building = mred_get_building($floor['building_id']);
			$plan = mred_get_plan($lot['plan_id']);

			$lot_link = get_permalink($lot['id']);
			if (function_exists('qtrans_convertURL')) {
				$lot_link = qtrans_convertURL($lot_link, $current_language);
			}

			$filter_classes = mred_get_plan_filter_classes($lot['id']);

			$image = get_stylesheet_directory_uri() . '/images/transparent.gif';

			if ($plan['detailed_plan_image']) {
				 $image_details = wp_get_attachment_image_src($plan['detailed_plan_image']['id'], 'plan-small');
				 $image = $image_details[0];
			}

			if ($plan['detailed_plan_image_fallback']) {
				 $image_details = wp_get_attachment_image_src($plan['detailed_plan_image_fallback']['id'], 'plan-small');
				 $fallback_image = $image_details[0];
			}


			// Availability / Price
			$availabiltyPrice = mred_get_availability_description($lot['availability']);

			if(isset($_SESSION['pricesEnabled']) && $_SESSION['pricesEnabled'] === true && $lot['availability'] == 'available'){
				if(isset($lot['price']) && $lot['price'] != '') {
					$availabiltyPrice = number_format((int)$lot['price'], 0, ".", "'");
					$availabiltyPrice = $availabiltyPrice.'CHF';
				}
			}

			$lot_code = apply_filters( 'mred_get_vignette_lot_code', $lot['code'], $lot );
			list($code_sector, $code_building, $code_floor, $code_lot) = explode('.', $lot['code']);

            $code_sector = '&Eacute;tape '.substr($code_sector,1);

			$separator = '<div class="vignette-separator">&nbsp;</div>';

			$floor_links = array();
			foreach ($lot['all_floors'] as $loop_floor_id => $loop_floor_details) {
				$loop_floor = mred_get_floor($loop_floor_id);
				$floor_links[] = '<a href="' . $floor_link . '#floor_' . $loop_floor_id . '">' . $loop_floor['ordinal'] . '</a>';
			}

			ob_start();

			?>

			<li class="apartment-list-image-wrapper <?php echo join(" ", $classes); ?> <?php echo join(" ", mred_get_lot_plan_classes()); ?> <?php echo join(" ", $filter_classes); ?>">
				<div class="apartment-list-image-inner">
					<div class="apartment-list-image">
						<div class="apartment_code"><a href="<?php echo $lot_link; ?>"><span class="lot_type"><?php echo $code_sector; ?> - <?php echo $lot['type']['name']; ?> </span><?php echo $lot_code; ?></a></div>
						<?php echo $separator; ?>
						<div class="apartment_details">

							<a href="<?php echo $building['url']; ?>">
								<span class="floor"><?php echo $building['name']; ?> <span class="separator">|</span></span>
							</a>

							<?php echo join(' - ', $floor_links); ?> <span class="separator">|</span>

							<a href="<?php echo $lot_link; ?>">
								<span class="rooms"><?php echo $lot['pieces']; echo '&nbsp;<span class="lower">' . mred_translate('p.') . '</span>'; ?> <span class="separator">|</span></span>
							</a>

							<a href="<?php echo $lot_link; ?>">
								<span class="area"><?php echo $lot['surface_weighted']; echo '&nbsp;<span class="lower">m</span><sup>2</sup>';  ?></span>
							</a>
                            <br/>
                            <a href="<?php echo $lot_link; ?>">
                                <span class="area"><?php echo get_field('availability_date', $building['id']) ; ?></span>
                            </a>
						</div>
						<div class="availability-wrapper"><a class="availability" href="<?php echo $lot_link; ?>"><?php echo $availabiltyPrice; ?></a></div>
						<div class="plan"><a href="<?php echo $lot_link; ?>"><img src="<?php echo $image; ?>" data-fallback="<?php echo $fallback_image; ?>" alt="<?php echo $lot['code']; ?>"></a></div>
						<div class="pdf-download"><?php echo mred_get_download_button($lot['id']); ?></div>
					</div>
					<a href="<?php echo $lot_link; ?>" class="apartment-list-overlay" style="display: none;">&nbsp;</a>
				</div>
			</li>

			<?php

			return apply_filters( 'mred_get_lot_markup_plan', ob_get_clean() );
		}
	}

	function mred_get_lot_breadcrumb($lot_id) {
		$lot = mred_get_lot($lot_id);
		$floor = mred_get_floor($lot['floor_id']);
		$building = mred_get_building($floor['building_id']);
		$sector = mred_get_sector($building['sector_id']);
		$development = mred_get_development($sector['development_id']);

		$list_page_id   =  mred_get_page_id_from_slug( PLAN_PAGE_SLUG );
		$list_page_link = get_permalink($list_page_id);

		$links = array(
			array(
				'title' => 'Plan de situation',
				'url'   => $development['url']
			),
			array(
				'title' => $building['name'],
				'url'   => $building['url'],
			),
			array(
				'title' => $lot['name'],
				'url'   => '#',
			)
		);

		$result = '<div class="breadcrumb">';

		$i = 1;
		$max  = count($links);
		foreach ($links as $link) {
			$result .= '<a href="' . $link['url'] .'">' . $link['title'] . '</a>';
			if($i < $max) {
				$result .= ' &gt; ';
			}
			$i++;
		}

		$result .= '</div>';

		return $result;
	}

	add_filter('plan_list_filter_options', 'gefi_plan_list_filter_options');

	function gefi_plan_list_filter_options($filter_menu_options) {
		foreach (array('sector', 'entry', 'lot_types', 'balcony') as $index) {
			unset($filter_menu_options[$index]);
		}

		$buildings = mred_get_buildings();

		$building_options = array();
		foreach ($buildings as $building) {
			$building_options[$building['id']] = $building['name'];
		}

		$filter_menu_options['building']['options'] = $building_options;

		return $filter_menu_options;
	}


	function mred_get_lot_label($lot, $availability_description, $floor_id) { ?>
		<div class="lot-and-rooms"><span class="lot_code"><?php echo $lot['name']; ?></span> | <span class="rooms"><?php echo $lot['pieces']; ?> <?php _e('p', MRED_TEXT_DOMAIN); ?></div>
		<?php if ($lot['multifloor']) {
			$order = $lot['all_floors'][$floor_id]['order'];
			$order_label = ($order == 1) ? '1<sup>er</sup>' : $order . '<sup>&egrave;me</sup>'
			?>
			<div class="duplex">Duplex - <?php echo $order_label; ?> &eacute;tage</div>
		<?php } ?>
		<div class="availability-label"><?php echo $availability_description; ?></div><?php
	}

	add_filter('mred_get_vignette_lot_code', 'gefi_get_vignette_lot_code', 10, 2);
	function gefi_get_vignette_lot_code($code, $lot) {
		$result = $lot['name'];
		if ($lot['multifloor']) {
			$result .= '<span class="duplex"> | duplex</span>';
		}
		return $result;
	}

#######################
/* Utility functions */
#######################
	add_action("login_head", "gefi_wp_login_head");
	function gefi_wp_login_head()
	{
	    echo "
		<style>
		body.login #login h1 a {
			background: url('" . get_stylesheet_directory_uri() . "/images/les-portes-du-lac-logo-login.png') no-repeat scroll center top transparent;
			height: 71px;
			width: 337px;
			margin-left: -12px;
			margin-bottom: 19px;
			padding: 0;
			border-radius: 6px;
		}
		body.login #login #nav {
			display: none;
		}
		</style>
		";
	}

	function gefi_encode_email( $email, $label = "" ) {
	  if (!is_email($email)) {
	    return $email;
	  }

	  if (empty($label)) {
	    $label = $email;
	  }

	  $ascii_label = unpack('C*', $label);
	  $ascii_email = unpack('C*', $email);

	  $mailto = "&#109;a&#105;l&#116;&#111;:"; // encoded "mailto:" string

	  $encoded_label = "&#" . join(";&#", $ascii_label) . ";";
	  $encoded_email = "&#" . join(";&#", $ascii_email) . ";";

	  return '<a href="' . $mailto . $encoded_email . '">' . $encoded_label . '</a>';
	}

/* Call To Action [ call_to_action_content | call_to_action_url | call_to_action_enabled ] */
	function gefi_get_active_call2action($post_id){

		$calls_to_action = get_field('call_to_action', $post_id);
		$calls = count($calls_to_action);
		// To collect only activated calls, if any
		$active_calls = array();
		$x = 0;

		if($calls > 0) {
			foreach ($calls_to_action as $call => $action) {
				if($action['call_to_action_enabled'] == 1) {
					$active_calls[$x]['content'] = $action['call_to_action_content'];
					$active_calls[$x]['url'] = '';
					$active_calls[$x]['url_text'] = '';
					// Something to link to
					/* Text field */
					if(!empty($action['call_to_action_url'])){
						$active_calls[$x]['url'] = $action['call_to_action_url'];
						$active_calls[$x]['url_text'] = $action['call_to_action_url_text'];
					}
					/* Change for this code, if they finally want the file field again
					if(is_array($action['call_to_action_url']) && array_key_exists('url', $action['call_to_action_url'])){
						$active_calls[$x]['url'] = $action['call_to_action_url']['url'];
						$active_calls[$x]['url_text'] = $action['call_to_action_url_text'];
					}
					*/
					$x++;
				}
			}
	  	}

	  	if($x > 0) {
	  		$i = rand(0, $x - 1);
	  		return $active_calls[$i];
	  	}
	  	else return false;
	}

/* Shortcode to generate located CalltoAction buttons [call2actionbtn title | url | target | class | linkclass ]
 * [call2actionbtn title="" url="" target="" class=""] */
	add_shortcode( 'call2actionbtn', 'call2action_shortcode' );

  	function call2action_shortcode( $atts ) {
	    $a = shortcode_atts( array(
	        'title' => '',
	        'url' => '#',
	    	'target' => '_self',
	    	'class' => '',
	    	'linkclass' => '',
	    ), $atts );

	    return '<div class="c2a-link-single '.$a['class'].'"><a href="'.$a['url'].'" target="'.$a['target'].'" class="'.$a['linkclass'].'">'.$a['title'].'</a></div>';
	}

/* Shortcode to generate input hidden [inputPreviousUrl url ]
 * [inputPreviousUrl url=""] */
	add_shortcode( 'inputPreviousUrl', 'inputPreviousUrl_shortcode' );

  	function inputPreviousUrl_shortcode() {
            $url = '';
            if(isset($_GET['current_url']) && !empty($_GET['current_url']))
            {
                $url = 'http://'.$_GET['current_url'];
            }
	    return '<input type="hidden" name="pervious_url" value="'.$url.'">';
	}
        
/* Shortcode to generate button cancel for file request page on mobile device [buttonCancelPreviousUrl url ]
 * [buttonCancelPreviousUrl url=""] */
        add_shortcode( 'buttonCancelPreviousUrl', 'buttonCancelPreviousUrl_shortcode' );

  	function buttonCancelPreviousUrl_shortcode() {
            
            $url = '';
            $html = '';
            
            if(isset($_GET['current_url']) && !empty($_GET['current_url']))
            {
                $url = 'http://'.$_GET['current_url'];
                $html = '<a class="wpcf7-button-cancel" href="'.$url.'"><input type="button" value="Annuler" /></a>';
            }
            
            return $html;
	}
        
/* Shortcode to generate button price list [buttonPriceList url]
 * [buttonPriceList url=""] */
        add_shortcode( 'buttonPriceList', 'buttonPriceList_shortcode' );

  	function buttonPriceList_shortcode($atts) {
            
            $a = shortcode_atts( array(
	            'url' => '#',
                'sector' => ''
	        ), $atts );
            
            require_once('class/Mobile_Detect.php');
            $detect = new Mobile_Detect();

            if($a['sector'] == 'c1') {
                $class_btn = 'btn-list-price-c1';
            }else if($a['sector'] == 'c2'){
                $class_btn = 'btn-list-price-c2';
            }else{
                $class_btn = 'btn-list-price';
            }
            
            if(!$detect->isMobile())
            {
                $class = 'fancybox-iframe btn-download-list-price';
                $url = $a['url'];
            }else{
                $current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
                $class = '';
                $url = $a['url'].'&current_url='.$current_url;
            }   
            
            $html  = '<a id="'.$class_btn.'" class="'.$class.'" href="'.$url.'">';
            $html .= '</a>';
            
            return $html;
	}
        
/* Shortcode to generate button price list [buttonPriceList url]
 * [buttonDownloadContrat url=""] */
        add_shortcode( 'buttonDownloadContrat', 'buttonDownloadContrat_shortcode' );

  	function buttonDownloadContrat_shortcode($atts) {
            
            $a = shortcode_atts( array(
	        'url' => '#'
	    ), $atts );
            
            require_once('class/Mobile_Detect.php');
            $detect = new Mobile_Detect();
            
            if(!$detect->isMobile())
            {
                $class = 'fancybox-iframe btn-download-contrat';
                $url = $a['url'];
            }else{
                $current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
                $class = '';
                $url = $a['url'].'&current_url='.$current_url;
            }   
            
            $html  = '<a id="btn-download-contrat" class="'.$class.'" href="'.$url.'">';            
            $html .= '</a>';
            
            return $html;
	}

	/* Shortcode to generate button newsletter [buttonnewsletter url] position : pos-header / pos-footer
	 * [buttonPriceList url=""] */
	add_shortcode( 'buttonnewsletter', 'buttonnewsletter_shortcode' );
	
	function buttonnewsletter_shortcode($atts) {
	
		$a = shortcode_atts( array(
				'url' => '#',
				'position' => 'pos-header'
		), $atts );
	
		// if(is_user_logged_in()){
		
			require_once('class/Mobile_Detect.php');
			$detect = new Mobile_Detect();
			
			if($a['position'] != 'pos-header') // we are in a widget so it's a text, no php to get the id
			{
				$class = 'fancybox-iframe ';
				$a['url'] = $a['url'].get_the_ID();
			}
		
			if(!$detect->isMobile())
			{
				$class = 'fancybox-iframe ';
				$url = $a['url'];
			}else{
				$current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
				$class = '';
				$url = $a['url'].'&current_url='.$current_url;
			}
		
			$html  = '<div class="btn-newsletter '.$a['position'].'"><a class="'.$class.'" href="'.$url.'">S\'abonner &agrave; la <span>NEWSLETTER</span></a></div>';
			
			if($a['position'] != 'pos-header') // Complement
			{
				// Uncomment when ready
				$html .= '<div class="btn-newsletter pos-footer" style="margin-top: 5px; display: block;"><a href="http://habiter-estavayer.ch/wp/wp-content/uploads/2016/01/LPDL-pres_publique20160116.pdf" target="_blank" class=""><span>PRESENTATION DU 20.01.16</span></a></div>';
			}
			
		
		// }
		// else $html = '';
		
		return $html;
	}
#############################################
/* Configure Advanced Custom Fields plugin */
#############################################
define('ACF_OPTION_PAGE_STUB', 'acf-options-estavayer-settings');

if( function_exists('acf_add_options_page') ) {
	// Note - if changing this, the ACF_OPTION_PAGE_STUB will need to change
	acf_add_options_page('Estavayer Settings');
}

if( function_exists('register_field_group') ) {

	register_field_group(array (
		'key' => 'group_54dccd889a11f',
		'title' => 'Estavayer Settings',
		'fields' => array (
			array (
				'key' => 'field_54dccdaa6f49f',
				'label' => 'Logo',
				'name' => 'logo',
				'prefix' => '',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'medium',
				'library' => 'all',
			),
			array (
				'key' => 'field_54dccddc6f4a0',
				'label' => 'Adresse',
				'name' => 'address',
				'prefix' => '',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 4,
				'new_lines' => 'br',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_54dcce096f4a1',
				'label' => 'Phone',
				'name' => 'phone',
				'prefix' => '',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_54dcce126f4a2',
				'label' => 'email',
				'name' => 'email',
				'prefix' => '',
				'type' => 'email',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
			array (
				'key' => 'field_54dcce1b6f4a3',
				'label' => 'Adresse web',
				'name' => 'url',
				'prefix' => '',
				'type' => 'text',
				'instructions' => 'Without http://',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => ACF_OPTION_PAGE_STUB,
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));
}

##############################################################################################################
/* Ensure we have the  mred javascript defined.  Delete these if enabling the meo-mred-pdf-generator plugin
##############################################################################################################
function mmpg_generate_scripts_callback($echo = false, $continue = false) {
	global $wpdb; // this is how you get access to the database

	// Get all the info from Database
	$dataToJS = array(
		'lots' => mred_get_lots(array('pdf', 'floor_position_image', 'floor_position_image_fallback', 'lot_situation_image', 'lot_situation_image_fallback', 'price')),
		'plans' => mred_get_plans(array('pdf')),
		'floors' => mred_get_floors(array('plan')),
		'sectors' => mred_get_sectors(),
		'buildings' => mred_get_buildings(),
		'lot_types' => mred_get_lot_types(),
		'entries' => mred_get_entries(),
		'translations' => array(
			'rooms' => mred_translate('Rooms'),
			'apartment' => mred_translate('Apartment'),
			'statuses' => mred_get_availability_descriptions()
		)
	);

	// Turn Data into JS code [No Price]
	$script = mmpg_dataToJS('mred', $dataToJS, true);

	// Get Script file path
	$upload_directory = wp_upload_dir();
	$destPath = $upload_directory['basedir'] . '/cache/';
	wp_mkdir_p( $destPath );

	$filePathNoPrice = $destPath.'script.txt';

	$cached = fopen($filePathNoPrice, 'w');
	fwrite($cached, $script);
	fclose($cached);

	// Echo the content (from header.php if file didn't exist (now it's created btw))
	if($echo){
		echo $script;
		return true;
	}
	else {
		# Commented below as it must not print anything during AJAX call
		// echo 'Fichers crees | Files created | Content cached';
		if($continue) return true; // Stop when updating from admin
		wp_die();
	}
}

function mmpg_dataToJS( $varName, $l10n, $fullscriptEmbed = false ) {

	// back compat, preserve the code in 'l10n_print_after' if present
	if ( is_array($l10n) && isset($l10n['l10n_print_after']) ) {
		$after = $l10n['l10n_print_after'];
		unset($l10n['l10n_print_after']);
	}

	foreach ( (array) $l10n as $key => $value ) {
		if ( !is_scalar($value) )
			continue;

		$l10n[$key] = html_entity_decode( (string) $value, ENT_QUOTES, 'UTF-8');
	}

	// Return
	$return = '';

	if($fullscriptEmbed){$return .= '<script type="text/javascript">/* <![CDATA[ *---------------------/'."\n";}

	$script = "var $varName = " . wp_json_encode( $l10n ) . ';'."\n";
	$return .= $script;

	if($fullscriptEmbed){$return .= '/* ]]> *-----------------------------------------------------------/</script>';}

	return $return;
}
 */
function estavayer_grid_display($atts, $content = null ) {

		$atts = shortcode_atts(
			array(
				'cats' => "",
				'posts' => "",
				'pages' => "",
				'external' => "",
				), $atts);

			/* Init */
			$final_container = array(); # Shuffled before to print / will collect final items

			/* CATEGORIES */
			$catsID = explode( ',', $atts['cats'] );

        	if(count($catsID) > 0) {
				foreach($catsID as $catID) {
					if($catID > 0) {
						$catObj = get_category($catID);
						$catID = $catObj->term_id;
						$catTitle = str_replace(array('/*', '*'), array('</span>', '<span class="grid-typo">'), __($catObj->description));
						$catimg = "<img src='".get_stylesheet_directory_uri()."/images/categories/".$catID.".jpg' alt='".$catTitle."' />";
						$catLink = get_category_link($catID);

						$final_container[] = array(
													'id' => $catID,
													'title' => $catTitle,
													'link' => $catLink,
													'type' => 'cat',
													'img' => $catimg
													);
					}
				}
        	}



			/* POSTS */
        	$postsID = explode( ',', $atts['posts'] );

        	if(count($postsID) > 0) {
        		$posts = get_posts( array( 'include' => $postsID ) );
        		if ( count($posts) > 0 ) {
					foreach($posts as $postObj) {
						$postID = $postObj->ID;
						if ( has_post_thumbnail($postID) ) {
							$postimg = get_the_post_thumbnail($postID, 'homepage-grid-square');
						}
						$categories = get_the_category($postID);
						$postTitle = get_the_title($postID);
						$postLink = get_the_permalink($postID);
						if($categories){
							$category = reset($categories);
							$postTitle = __($category->description);
							$postLink = get_category_link($category->term_id);
						}
						$final_container[] = array(
													'id' => $postID,
													'title' => str_replace(array('/*', '*'), array('</span>', '<span class="grid-typo">'), $postTitle),
													'link' => $postLink,
													'type' => 'post',
													'img' => $postimg
													);
					}
				}
        	}

        	/* PAGES */
        	$pagesID = explode( ',', $atts['pages'] );

			if(count($pagesID) > 0) {
        		$pages = get_pages( array( 'include' => $pagesID ) );
        		if ( count($pages) > 0 ) {
					foreach($pages as $pageObj) {
						$pageID = $pageObj->ID;
						if ( has_post_thumbnail($pageID) ) {
							$pageimg = get_the_post_thumbnail($pageID, 'homepage-grid-square');
						}
						if(!empty($pageObj->post_excerpt)){
							$pageTitle = __($pageObj->post_excerpt);
						} else $pageTitle = get_the_title($pageID);
						$final_container[] = array(
													'id' => $pageID,
													'title' => str_replace(array('/*', '*'), array('</span>', '<span class="grid-typo">'), $pageTitle),
													'link' => get_the_permalink($pageID),
													'type' => 'page',
													'img' => $pageimg
													);
					}
				}
        	}

			/* EXTERNAL PAGES */
        	$pagesID = explode( ',', $atts['external'] );

			if(count($pagesID) > 0) {
        		$pages = get_pages( array( 'include' => $pagesID ) );
        		if ( count($pages) > 0 ) {
					foreach($pages as $pageObj) {
						$pageID = $pageObj->ID;
						if ( has_post_thumbnail($pageID) ) {
							$pageimg = get_the_post_thumbnail($pageID, 'homepage-grid-square');
						}
						if(!empty($pageObj->post_excerpt)){
							$pageTitle = __($pageObj->post_excerpt);
						} else $pageTitle = get_the_title($pageID);
						$final_container[] = array(
													'id' => $pageID,
													'title' => str_replace(array('/*', '*'), array('</span>', '<span class="grid-typo">'), $pageTitle),
													'link' => __($pageObj->post_content),
													'type' => 'external',
													'img' => $pageimg
													);
					}
				}
        	}

        	shuffle($final_container);


        	/* DOWNLOAD */
        	$final_container[] = array(
										'id' => $pageID,
										'title' => __('[:fr]<span class="grid-typo">T&eacute;l&eacute;charger le</span> Portfolio[:en]<span class="grid-typo">T&eacute;l&eacute;charger le</span> Portfolio[:de]<span class="grid-typo">T&eacute;l&eacute;charger le</span> Portfolio'),
										'link' => '#',
										'type' => 'portfolio',
										'img' => '<div class="download-icon"></div>'
										);

			$html = '<div id="main-grid-container">';

				foreach($final_container as $gridItem){

					$external = '';
					if($gridItem['type'] == 'external'){$external = ' target="_blank"';}

					$html.= '<div class="grid-item item-'.$gridItem['id'].' item-type-'.$gridItem['type'].'">';
						$html.= '<a class="grid-item-img" href="'.$gridItem['link'].'"'.$external.'>';
						if( $gridItem['type'] != 'portfolio'){$html.= '<div class="grid-item-img">'.$gridItem['img'].'</div>';}
						else $html.= '<div class="grid-item-img-dl">'.$gridItem['img'].'</div>';
						$html.= '<div class="grid-item-title">'.$gridItem['title'].'</div>';

						$under = mt_rand(0, 9);
						if($under < 6){$html.= '<div class="grid-item-under"></div>';}

						$html.= '</a>';
					$html.= '</div>';
				}

			$html.= '</div>';

			return $html;

}

add_shortcode('estavayergrid', 'estavayer_grid_display');

# ORDER GRID #

function estavayer_grid_display_order($atts, $content = null ) {
	// [estavayergridorder list="cat:1:0:1,page:1223:1:0,cat:4:0:1,external:1014:0:1,page:711:0:1,cat:5:0:0,cat:3:0:1,page:992:0:1,cat:7:0:0,page:1020:0:1,cat:6:0:1,page:1194:0:0"]
	// Atts structure
	# "post:id:round:line,cat:id:round:line,....."

		# post: string (post,cat,page,external)
		# id: integer
		# round: 1/0
		# line: 1/0

	// list="cat:1:0:1,page:1223:1:0,cat:4:0:1,external:1014:0:1,page:711:0:1,cat:5:0:0,cat:3:0:1,page:992:0:1,cat:7:0:0,page:1020:0:1,cat:6:0:1,page:1194:0:0"

	$atts = shortcode_atts(
		array(
			'list' => "",
			'class' => "",
			), $atts);

		/* Init */
		$final_container = array(); # Shuffled before to print / will collect final items


		$listElements = explode( ',', $atts['list'] );
		$classElements = $atts['class'];

		foreach($listElements as $listElement){

			$elementInfo = explode(':', $listElement);

			$type = $elementInfo[0];
			$itId = $elementInfo[1];
			$round = $elementInfo[2];
			$line = $elementInfo[3];

			# Init / Reset
			$itemImg = '';
			$itemTitle = '';
			$itemLink = '#';
			$isdownload = false;

			# POST
			if($type == 'post'){ # TODO: Review if prints post info or related category
				if ( has_post_thumbnail($itId) ) {
					$itemImg = get_the_post_thumbnail($itId, 'homepage-grid-square');
				}
				$categories = get_the_category($itId);
				$itemTitle = str_replace(array('/*', '*'), array('</span>', '<span class="grid-typo">'), get_the_title($itId));
				$itemLink = get_the_permalink($itId);
				if($categories){
					$category = reset($categories);
					$itemTitle = __($category->description);
					$itemLink = get_category_link($category->term_id);
				}
			}
			# PAGE || EXTERNAL LINK
			else if($type == 'page' || $type == 'external' || $type == 'pagedownload'){
				$pageObj = get_page($itId);
				$pageID = $pageObj->ID;
				if ( has_post_thumbnail($pageID) ) {
					$itemImg = get_the_post_thumbnail($pageID, 'homepage-grid-square');
				}
				if(!empty($pageObj->post_excerpt)){
					$itemTitle = __($pageObj->post_excerpt);
				} else $itemTitle = get_the_title($pageID);
				$itemTitle = str_replace(array('/*', '*'), array('</span>', '<span class="grid-typo">'), $itemTitle);
				$itemLink = get_the_permalink($pageID);
				
				if($type == 'pagedownload') {
					$isdownload = true;
				}
			}
			# EXTERNAL LINK
			#else if($type == 'external'){
			#
			#}
			# CATEGORY
			else if($type == 'cat'){
				$catObj = get_category($itId);
				$itemTitle = str_replace(array('/*', '*'), array('</span>', '<span class="grid-typo">'), __($catObj->description));
				$itemImg = "<img src='".get_stylesheet_directory_uri()."/images/categories/".$itId.".jpg' alt='".$itemTitle."' />";
				$itemLink = get_category_link($itId);

			}

			$final_container[] = array(
										'id' => $itId,
										'title' => $itemTitle,
										'link' => $itemLink,
										'type' => $type,
										'img' => $itemImg,
										'round' => $round,
										'line' => $line,
										'download' => $isdownload
										);
		}

        /* DOWNLOAD
        $final_container[] = array(
									'id' => $pageID,
									'title' => __('[:fr]<span class="grid-typo">T&eacute;l&eacute;charger le</span> Portfolio[:en]<span class="grid-typo">T&eacute;l&eacute;charger le</span> Portfolio[:de]<span class="grid-typo">T&eacute;l&eacute;charger le</span> Portfolio'),
									'link' => '#',
									'type' => 'portfolio',
									'img' => '<div class="download-icon"></div>'
									);
		 */

		$html = '<div id="main-grid-container" class="'.$classElements.'">';
		
			# Responsive?
			if(strpos($classElements, 'responsive') !== false) {$html .= '<div class="responsive-grid-left">';}
			
			$index = 0;
			foreach($final_container as $gridItem){

				$index++;

				$external = '';
				if($gridItem['type'] == 'external'){$external = ' target="_blank"';}

				$html.= '<div class="grid-item item-'.$gridItem['id'].' item-type-'.$gridItem['type'].'">';
					$html.= '<a class="grid-item-img" href="'.$gridItem['link'].'"'.$external.'>';

					if( $gridItem['type'] != 'portfolio'){$html.= '<div class="grid-item-img">'.$gridItem['img'].'</div>';}
					else $html.= '<div class="grid-item-img-dl">'.$gridItem['img'].'</div>';

					if($gridItem['download']){$html.= '<div class="grid-item-img-dl"><div class="download-icon"></div></div>';}

					if($gridItem['round'] == 1 ){$html.= '<div class="grid-item-round"></div>';}


					$html.= '<div class="grid-item-title">'.$gridItem['title'].'</div>';

					if($gridItem['line'] == 1 ){$html.= '<div class="grid-item-under"></div>';}

					$html.= '</a>';
				$html.= '</div>';
				
				if(strpos($classElements, 'responsive') !== false && $index == 6) {$html .= '</div><div class="responsive-grid-right">';}
			}
			
			if(strpos($classElements, 'responsive') !== false) {$html .= '</div>';}

		$html.= '</div>';

		return $html;

}

add_shortcode('estavayergridorder', 'estavayer_grid_display_order');

function estavayer_cat_image() {

			$category = reset(get_the_category());
			$html = '<div id="side-cat-img-container">
						<div class="grid-item">';
				$html .= "<div class='grid-item-img'><img src='".get_stylesheet_directory_uri()."/images/categories/".$category->term_id.".jpg' /></div>";
				$html.= '<div class="grid-item-title">'.str_replace(array('/*', '*'), array('</span>', '<span class="grid-typo">'), __($category->description)).'</div>';
			$html.= '</div>
					</div>';

			return $html;

}

add_shortcode('cat_image', 'estavayer_cat_image');

function mk_header_main_navigation($arg) {
		global $mk_options;
		extract($arg);
		$output = $main_nav = '';
		$post_id = global_get_post_id();
		$post_id = mk_is_woo_archive() ? mk_is_woo_archive() : $post_id;
		$single_menu_location = !empty($post_id) ? get_post_meta($post_id, '_menu_location', true) : false;

		if ($post_id && isset($single_menu_location) && !empty($single_menu_location)) {

			$menu_location = $single_menu_location;

		} else {

			if (is_user_logged_in() && !empty($mk_options['loggedin_menu'])) {
				$menu_location = $mk_options['loggedin_menu'];
			} else {
				$menu_location = 'primary-menu';
			}

		}

		$search_form = '<div class="main-nav-side-search">
						<a class="mk-search-trigger mk-toggle-trigger" href="#"><i class="mk-icon-search"></i></a>
							<div id="mk-nav-search-wrapper" class="mk-box-to-trigger">
							      <form method="get" id="mk-header-navside-searchform" action="' . home_url() . '">
							        <input type="text" value="" name="s" id="mk-ajax-search-input" />
							        <i class="mk-moon-search-3 nav-side-search-icon"><input value="" type="submit" /></i>
							    </form>
					  		</div>
					</div>';

		$search_form_fullscreen = '<div class="main-nav-side-search">
										<a class="mk-search-trigger mk-fullscreen-trigger" href="#"><i class="mk-icon-search"></i></a>
									</div>';

		if ($style == 1 && $nav_location == 'one_row'):

			echo '<div class="mk-header-nav-container one-row-style menu-hover-style-' . $mk_options['main_nav_hover'] . '">';
			echo wp_nav_menu(array(
				'theme_location' => $menu_location,
				'container' => 'nav',
				'container_id' => 'mk-main-navigation',
				'container_class' => 'main_menu',
				'menu_class' => 'main-navigation-ul',
				'echo' => false,
				'fallback_cb' => 'link_to_menu_editor',
				'walker' => new mk_artbees_walker,
				'link_after' => '<span></span>',
			));

			if ($search_location == 'beside_nav' || $search_location == 'beside_nav_no_ajax') {
				echo $search_form;
			} elseif ($search_location == 'fullscreen_search') {
			echo $search_form_fullscreen;
		}

		ob_start();
		do_action('header_checkout');
		ob_end_flush();

		echo '</div>';

		elseif ($style == 2 && $nav_location == 'two_row'):

			echo '<div class="mk-header-nav-container menu-hover-style-' . $mk_options['main_nav_hover'] . '">
					  						<div class="mk-classic-nav-bg"></div>
					  						<div class="mk-classic-menu-wrapper">';

			echo wp_nav_menu(array(
				'theme_location' => $menu_location,
				'container' => 'nav',
				'container_id' => 'mk-main-navigation',
				'container_class' => 'main_menu',
				'menu_class' => 'main-navigation-ul',
				'echo' => false,
				'fallback_cb' => 'link_to_menu_editor',
				'walker' => new mk_artbees_walker,
				'link_after' => '<span></span>',
			));

			if ($search_location == 'beside_nav') {
				echo $search_form;
			} elseif ($search_location == 'fullscreen_search') {
			echo $search_form_fullscreen;
		}

		ob_start();
		do_action('header_checkout');
		ob_end_flush();

		echo '</div></div>';
		endif;

	}

add_action('header_logo_home', 'mk_header_logo_home');
function mk_header_logo_home() {
		global $mk_options;

		$logo = $mk_options['logo'];
		$mobile_logo = $mk_options['responsive_logo'];
		$light_logo = isset($mk_options['light_header_logo']) ? $mk_options['light_header_logo'] : '';
		$sticky_logo = $mk_options['sticky_header_logo'];
		$footer_logo = $mk_options['footer_logo'];

		$post_id = global_get_post_id();

		if ($post_id) {
			global $post;
			$enable = get_post_meta($post_id, '_enable_local_backgrounds', true);

			if ($enable == 'true') {
				$logo_meta = get_post_meta($post_id, 'logo', true);
				$sticky_logo_meta = get_post_meta($post_id, 'sticky_header_logo', true);
				$light_logo_meta = get_post_meta($post_id, 'light_logo', true);
				$logo_mobile_meta = get_post_meta($post_id, 'responsive_logo', true);
				$logo = (isset($logo_meta) && !empty($logo_meta)) ? $logo_meta : $logo;
				$mobile_logo = (isset($logo_mobile_meta) && !empty($logo_mobile_meta)) ? $logo_mobile_meta : $mobile_logo;
				$sticky_logo = (isset($sticky_logo_meta) && !empty($sticky_logo_meta)) ? $sticky_logo_meta : $sticky_logo;
				$light_logo = (isset($light_logo_meta) && !empty($light_logo_meta)) ? $light_logo_meta : $light_logo;
			}
		}

		if(!empty($footer_logo)) {$logo = $footer_logo;} # To have a different logo on the homepage (quick fix)

		if (!empty($logo) || !empty($sticky_logo)) {

			$is_repsonive_logo = !empty($mobile_logo) ? 'logo-is-responsive' : '';
			$is_sticky_logo = !empty($sticky_logo) ? 'logo-has-sticky' : '';

			?>
		<div class="header-logo <?php echo $is_repsonive_logo . ' ' . $is_sticky_logo;?>">
		    <a href="<?php echo home_url('/');?>" title="<?php bloginfo('name');?>">

				<img style="margin: 0 auto; width:280px;" alt="<?php bloginfo('name');?>" src="<?php echo $logo;?>" />

				</a>
						</div>

				<?php } else {?>
						<div class="header-logo">
						    <a href="<?php echo home_url('/');?>" title="<?php bloginfo('name');?>"><img alt="<?php bloginfo('name');?>" src="<?php echo THEME_IMAGES;?>/jupiter-logo.png" /></a>
						</div>
				<?php }

		}
		
	function mk_header_logo() {
		global $mk_options;

		$logo = $mk_options['logo'];
		$mobile_logo = $mk_options['responsive_logo'];
		$light_logo = isset($mk_options['light_header_logo']) ? $mk_options['light_header_logo'] : '';
		$sticky_logo = $mk_options['sticky_header_logo'];

		$post_id = global_get_post_id();

		if ($post_id) {
			global $post;
			$enable = get_post_meta($post_id, '_enable_local_backgrounds', true);

			if ($enable == 'true') {
				$logo_meta = get_post_meta($post_id, 'logo', true);
				$sticky_logo_meta = get_post_meta($post_id, 'sticky_header_logo', true);
				$light_logo_meta = get_post_meta($post_id, 'light_logo', true);
				$logo_mobile_meta = get_post_meta($post_id, 'responsive_logo', true);
				$logo = (isset($logo_meta) && !empty($logo_meta)) ? $logo_meta : $logo;
				$mobile_logo = (isset($logo_mobile_meta) && !empty($logo_mobile_meta)) ? $logo_mobile_meta : $mobile_logo;
				$sticky_logo = (isset($sticky_logo_meta) && !empty($sticky_logo_meta)) ? $sticky_logo_meta : $sticky_logo;
				$light_logo = (isset($light_logo_meta) && !empty($light_logo_meta)) ? $light_logo_meta : $light_logo;
			}
		}

		if (!empty($logo) || !empty($sticky_logo)) {

			$is_repsonive_logo = !empty($mobile_logo) ? 'logo-is-responsive' : '';
			$is_sticky_logo = !empty($sticky_logo) ? 'logo-has-sticky' : '';

			?>
		<div class="header-logo <?php echo $is_repsonive_logo . ' ' . $is_sticky_logo;?>">
		    <a href="<?php echo home_url('/');?>" title="<?php bloginfo('name');?>">

				<img class="mk-desktop-logo dark-logo" alt="<?php bloginfo('name');?>" src="<?php echo $logo;?>" />


<?php if ($light_logo) {?>
				<img class="mk-desktop-logo light-logo" alt="<?php bloginfo('name');?>" src="<?php echo $light_logo;?>" />
<?php }?>

<?php if ($mobile_logo) {?>
				<img class="mk-resposnive-logo" alt="<?php bloginfo('name');?>" src="<?php echo $mobile_logo;?>" />
<?php }?>

<?php if ($sticky_logo) {?>
				<img class="mk-sticky-logo" alt="<?php bloginfo('name');?>" src="<?php echo $sticky_logo;?>" />
<?php }?>
</a>
		</div>

<?php } else {?>
		<div class="header-logo">
		    <a href="<?php echo home_url('/');?>" title="<?php bloginfo('name');?>"><img alt="<?php bloginfo('name');?>" src="<?php echo THEME_IMAGES;?>/jupiter-logo.png" /></a>
		</div>
<?php }

	}


// Get download button HTML
function mred_get_download_button($lot_id, $type_page=0) {
	global $download_lot_id;
	$download_lot_id = $lot_id;
        
	// Appel de la class mobile detect qui permet de detecter avec quel device on se connect au site
	require_once('class/Mobile_Detect.php');
	$detect = new Mobile_Detect;
        
	$lot = mred_get_lot($lot_id);

	if($detect->isMobile())
	{

	}

	if (!empty($lot['pdf'])) {

            if($detect->isMobile())
            {

                $current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
                $additional_parameters = 'current_url='.$current_url.'';
                
                if($type_page == 0)
                {
                    return '<a class="pdf-download-link" href="' .  mred_get_pdf_request_url($lot_id,$additional_parameters) . '">' . mred_translate('Download Info pack') .'</a>';
                }else{
                    return '<a class="pdf-download-link" href="' .  mred_get_pdf_request_url($lot_id,$additional_parameters) . '"></a>';
                }

            }else{

                $current_url = '';
                $additional_parameters = 'current_url='.$current_url.'';
                
                if($type_page == 0)
                {
                    return '<a class="fancybox-iframe pdf-download-link" href="' .  mred_get_pdf_request_url($lot_id,$additional_parameters) . '">' . mred_translate('Download Info pack') .'</a>';
                }else{
                    return '<a class="fancybox-iframe pdf-download-link" href="' .  mred_get_pdf_request_url($lot_id,$additional_parameters) . '"></a>';
                }

            }

	}

	$lot_link = get_permalink($lot_id);
	return '<a class="see-lot-link" href="' . $lot_link .'">' . __('[:fr]T&eacute;l&eacute;charger[:en]Download[:de]Download') .' ' . __('[:fr]dossier complet[:en]complete file[:de]complete file') .'</a>';
}


/**
 * This function will connect wp_mail to your authenticated
 * SMTP server. This improves reliability of wp_mail, and 
 * avoids many potential problems.
 *
 * Author:     Chad Butler
 * Author URI: http://butlerblog.com
 *
 * For more information and instructions, see:
 * http://b.utler.co/Y3
 */
add_action( 'phpmailer_init', 'send_smtp_email' );
function send_smtp_email( $phpmailer ) {

	// Define that we are sending with SMTP
	$phpmailer->isSMTP();

	// The hostname of the mail server
	$phpmailer->Host = "mail.infomaniak.ch";

	// Use SMTP authentication (true|false)
	$phpmailer->SMTPAuth = true;

	// SMTP port number - likely to be 25, 465 or 587
	$phpmailer->Port = "587";

	// Username to use for SMTP authentication
	$phpmailer->Username = "webmaster@habiter-estavayer.ch";

	// Password to use for SMTP authentication
	$phpmailer->Password = "w3bEst@!mail!";

	// Encryption system to use - ssl or tls
	$phpmailer->SMTPSecure = "tls";

	$phpmailer->From = "no-reply@habiter-estavayer.ch";
	$phpmailer->FromName = "Habiter Estavayer";
	
	// print_r($phpmailer);
}
/* Theme Function for isMobile */
function gefiswiss_isMobile(){
	require_once('class/Mobile_Detect.php');
	$detect = new Mobile_Detect();
	return $detect->isMobile();
}

/* Shortcode to generate located CalltoAction buttons [call2actionbtn title | url | target | class | linkclass ]
 * [homepage_popup] */
	add_shortcode( 'homepage_popup', 'homepage_popup_shortcode' );

  	function homepage_popup_shortcode( $atts ) {
            
            $html  = '';
            
            $html .= '<div id="popup-container">';
            $html .=    '<div id="popup-content">';
            $html .=        '<div class="top-popup"><img width="250" src="http://habiter-estavayer.ch/wp/wp-content/uploads/2015/06/LOGOporteslac_size1.png" alt="Les portes du lac" /></div>';
            $html .=        '<h2>Séance d’information publique | 20 janvier 2016 - 19h</h2>';            
            $html .=        '<div class="row">';
            $html .=            '<div class="col-md-6">';
            $html .=                '<p class="bluecolor meo-uppercase meo-bolder">Un nouveau quartier<br/>d\'Estavayer-le-lac</p>';
            $html .=                '<p class="meo-bolder">Le nouveau quartier des Portes du Lac va améliorer considérablement la qualité de vie de notre cité, grâce à son immense parc public et à ses commerces ! Vous êtes invités à découvrir le projet avant le début des travaux.</p>';
            $html .=                '<p>Nous vous invitons à une séance d’information publique le mercredi 20 janvier à 19h à la <a href="https://goo.gl/maps/T6LW2bTLW142" title="" target="_blank" >salle communale de La Prillaz</a> à Estavayer-le-Lac.</p>';
            $html .=                '<p>Les intervenants seront:<br/>Boris Clivaz, Maître d’ouvrage<br/>Vincent Mavilia, architecte du projet<br/>André Losey, Syndic d’Estavayer-le-Lac<br/>Jérôme Ramelet, habitat durable eSMART</p>';
            $html .=                '<p class="greencolor">Inscrivez-vous jusqu’au 19 janvier par téléphone au 021 613 80 70 ou par le formulaire.</p>';
            $html .=            '</div>';
            $html .=            '<div class="col-md-6">';
            $html .=                do_shortcode('[contact-form-7 id="7075" title="Porte ouverte"]');
            $html .=            '</div>';
            $html .=        '</div>';
            $html .=    '</div>';
            $html .= '</div>';
            
            return $html;
	}

/* Shortcode to generate located CalltoAction buttons [call2actionbtn title | url | target | class | linkclass ]
* [btn_sidebar_appartement] */
add_shortcode( 'btn_sidebar_appartement', 'btn_sidebar_appartement_shortcode' );

function btn_sidebar_appartement_shortcode( $atts ) {

    $html  =    '<div id="btn_sidebar_appartement" style="background-color: #b6ada5;background-image: url(http://habiter-estavayer.meomeo.ch/wp/wp-content/uploads/2017/07/LPDL_Appartement.jpg);background-repeat: no-repeat;background-position: center center;">';
    $html .=        '<a style="background-color:#0094d7;" href="http://habiter-estavayer.meomeo.ch/plans-par-liste/" alt="Découvrir les appartements">Découvrir les appartements</a>';
    $html .=    '</div>';

    return $html;
}

function orderLotsAvailability($a, $b){
    if ($a["availability"] == $b["availability"]) {  //here pass your index name
        return 0;
    }
    return ($b["availability"] > $a["availability"]) ? -1 : 1;    // this your key "value" will be in descending order as  requirement
    //but if you want to change this order from descending to Ascending order so just change the -1 and 1 place in above condition
}


function orderLotsName($a, $b){
    if ($a["name"] == $b["name"]) {  //here pass your index name
        return 0;
    }
    return ($a["name"] > $b["name"]) ? -1 : 1;    // this your key "value" will be in descending order as  requirement
    //but if you want to change this order from descending to Ascending order so just change the -1 and 1 place in above condition
}