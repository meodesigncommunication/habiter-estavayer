<?php 
/**
 * Template Name: Page Grid
 */
global $post,
$mk_options;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );


if ( empty( $page_layout ) ) {
	$page_layout = 'full';
}
$padding = ($padding == 'true') ? 'no-padding' : '';

$grid_elements = get_field('grid_elements', $post->ID);
$content_elements = get_field('content_elements', $post->ID);

get_header('grid'); ?>
<div id="theme-page">
	<?php /* Call To Action [ call_to_action_content | call_to_action_url | call_to_action_enabled ] */
	$active_call = gefi_get_active_call2action($post->ID);
	
	if($active_call) {
		?>
		<div id="call-to-action">
			<div class="c2a-container">
			    <div class="c2a-content"><?php echo __($active_call['content']); ?></div>
			    <?php if(!empty($active_call['url'])){ ?><div class="c2a-link"><a href="<?php echo $active_call['url']; ?>"><?php echo __($active_call['url_text']); ?></a></div><?php } ?>
		    </div>
	  	</div>
  	<?php 
  	}
  	/* End Call To Action */ ?>
  	
	<div class="mk-main-wrapper-holder">
		<div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper <?php echo $page_layout; ?>-layout <?php echo $padding; ?> mk-grid vc_row-fluid">
			<div class="theme-content <?php echo $padding; ?>" itemprop="mainContentOfPage">
                            <?php 
                            $info_txt = get_field( "info_texte", $post->ID );
                            $info_color = get_field( "info_color", $post->ID );
                            if(!empty($info_txt)):                                 
                            ?>
                            <a href="http://habiter-estavayer.ch/plans-par-liste/" title="Contact"><div id="banner-info-home" style="background-color: <?php echo $info_color; ?>;"><p style="letter-spacing: 1px;color:#fff;text-align: center;font-weight: bold; text-transform: uppercase"><?php echo $info_txt; ?></p></div></a>
                            <?php endif; ?>
				<?php if ( have_posts() ) while ( have_posts() ) : the_post();?>

                    <div class="flex-row">
                        <?php foreach($grid_elements as $grid_element): ?>
                            <?php
                                $style = '';
                                $style_width = 'width: calc('.$grid_element['col_size'].'% - 4px);';
                                if($grid_element['background_type'] == 'image') {
                                    $style .= 'background-image: url('.$grid_element['background_image'].');';
                                    $style .= 'background-repeat: no-repeat;';
                                    $style .= 'background-position: '.$grid_element['vertical_align'].' '.$grid_element['horizontal_align'].';';
                                    $class = 'image-bg-element';
                                }else{
                                    $style .= 'background-color: '.$grid_element['background_color'].';';
                                    $class = 'color-bg-element';
                                }
                            ?>
                            <div class="element-grid <?php echo $class.' '.$grid_element['classe']; ?>" style="<?php echo $style_width; ?>">
                                <div class="inner-element" style="<?php echo $style; ?>"></div>
                                <a href="<?php echo $grid_element['link']; ?>" title="<?php echo $grid_element['label']; ?>" <?php echo ($grid_element['link_type'] == 'blank') ? 'target="_blank"' : ''; ?>>
                                    <span>
                                        <?php if(!empty($grid_element['icon'])): ?>
                                            <i class="fa <?php echo $grid_element['icon'] ?>"></i>
                                            <br/>
                                        <?php endif; ?>
                                        <?php echo $grid_element['label']; ?>
                                    </span>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php
                    $btn_texte = get_field( "btn_texte", $post->ID );
                    $btn_color = get_field( "btn_color", $post->ID );
                    $btn_link = get_field( "btn_link", $post->ID );
                    if(!empty($info_txt)):
                        ?>
                        <a href="<?php echo $btn_link ?>" title="Contact"><div id="banner-info-home" class="new" style="margin-top: 8px; background-color: <?php echo $btn_color; ?>;"><p style="letter-spacing: 1px;color:#fff;text-align: center;font-weight: bold; text-transform: uppercase"><?php echo $btn_texte; ?></p></div></a>
                    <?php endif; ?>
                    <div class="clearboth"></div>
                    <?php wp_link_pages( 'before=<div id="mk-page-links">'.__( 'Pages:', 'mk_framework' ).'&after=</div>' ); ?>
				<?php endwhile; ?>
			</div>

            <div class="row-content">
                <?php foreach($content_elements as $content_element): ?>
                    <?php

                        $style .= 'background-image: url('.$content_element['background_image'].');';
                        $style .= 'background-repeat: no-repeat;';
                        $style .= 'background-position: '.$content_element['vertical_align'].' '.$content_element['horizontal_align'].';';

                    $html = '';

                        if(!empty($content_element['button_label'])) {

                            $html  = '<a onmouseover="this.style.background=\'#fff\';this.style.color=\''.$content_element['color_button'].'\';" onmouseout="this.style.background=\''.$content_element['color_button'].'\';this.style.color=\'#fff\';" style="background-color:'.$content_element['color_button'].';border-color:'.$content_element['color_button'].';" href="'.$content_element['button_url'].'" alt="'.$content_element['button_label'].'">';
                            $html .=    $content_element['button_label'];
                            $html .= '</a>';

                        }
                    ?>
                    <article class="content-element">
                        <div>
                            <h1><?php echo $content_element['title']; ?></h1>
                            <?php echo $content_element['content']; ?>
                        </div>
                        <div style="<?php echo $style; ?>">
                            <?php echo $html ?>
                        </div>
                    </article>
                <?php endforeach; ?>
            </div>

			<?php
				if(isset($mk_options['pages_comments']) && $mk_options['pages_comments'] == 'true') {
					comments_template( '', true ); 	
				}
			?>
		<?php if ( $page_layout != 'full' ) get_sidebar(); ?>
		<div class="clearboth"></div>
		</div>
		<div class="clearboth"></div>
	</div>	
</div>
<?php get_footer('grid'); ?>
