<?php
/**
 * Single Development Template
 */

add_filter( 'wp_title', 'mredtemplate_dev_page_title', 10, 2 );
function mredtemplate_dev_page_title( $title, $sep ) {
	return $sep . 'plan masse';
}

function mred_get_dev_for_building($building) {
	// Buildings < Sector < Dev
	if ($building['sector_id']) {
		$sector = mred_get_sector($building['sector_id']);
		return $sector['development_id'];
	}
	return -1;
}

mred_show_page_header();

$development = mred_get_development(get_the_ID());

/* Check if mobile or not */
$mobVar = '';
if( gefiswiss_isMobile() ){$mobVar = '_mobile';}

$plan = $development['plan'.$mobVar];
$plan_hover_transparent = $development['plan_hover_transparent']; /* For now, we suppose that mobile and non mobile plan, have same dimensions */

$buildings = mred_get_buildings();

$imagemappolygons = array();
foreach ($buildings as $building) {
	if (empty($building['dev_plan'.$mobVar.'_coordinates'])) {
		continue;
	}
	if (mred_get_dev_for_building($building) != $development['id']) {
		continue;
	}
	$imagemappolygons[] = array(
		'name' => $building['name'],
		'slug' => $building['slug'],
		'coordinates' => $building['dev_plan'.$mobVar.'_coordinates'],
		'url' => get_permalink($building['id']),
		'hover-image' => $building['dev_plan_hover_image']['url'] /* On mobile, hover doesn t work, so we can keep this one (so far) */
	);
}

$floors = mred_get_floors();
foreach ($floors as $floor) {
	if (empty($floor['dev_plan_coordinates'])) {
		continue;
	}
	$building = mred_get_building($floor['building_id']);
	if (mred_get_dev_for_building($building) != $development['id']) {
		continue;
	}
	$imagemappolygons[] = array(
		'name' => $building['name'],
		'slug' => $floor['slug'],
		'coordinates' => $floor['dev_plan_coordinates'],
		'url' => get_permalink($building['id']) . '#floor_' . $floor['id'],
		'hover-image' => $floor['dev_plan_hover_image']['url']
	);
}
$usemap = !empty($imagemappolygons);

$lot_list_page_id = mred_get_page_id_from_slug(PLAN_PAGE_SLUG);
$lot_list_page = get_permalink($lot_list_page_id);

?>
<div id="theme-page">
	<div class="theme-page-wrapper vc_row-fluid mk-grid row-fluid">
		<div class="theme-content">
			<div class="view-switchers">
				<span class="label"><?php _e('Display by', MREDTEMPLATES_TEXT_DOMAIN); ?></span>
				<a href="<?php echo $lot_list_page; ?>#apartment-images-content" class="view-switcher apartment-images" data-analytics-id="apartment-images">images</a>
				<a href="<?php echo $lot_list_page; ?>#apartment-list-content" class="view-switcher apartment-list" data-analytics-id="apartment-list">list</a>
			</div>
			<div class="page-content"><?php the_content(); ?></div>
			<div class="image-wrapper-outer">
				<div class="image-wrapper">
					<img class="plan" src="<?php echo $plan['url']; ?>" width="<?php echo $plan['width']; ?>" height="<?php echo $plan['height']; ?>" alt="<?php the_title(); ?>" />
					<img class="plan-overlay-transparent" src="<?php echo $plan_hover_transparent['url']; ?>" width="<?php echo $plan_hover_transparent['width']; ?>" height="<?php echo $plan_hover_transparent['height']; ?>" alt="<?php the_title(); ?>"<?php echo $usemap ? ' usemap="#buildings"': ''; ?> />

					<?php if ($usemap) { ?>
						<map id="buildings" name="buildings">
							<?php foreach ($imagemappolygons as $imagemappolygon) { ?>
								<area href="<?php echo $imagemappolygon['url']; ?>" shape="polygon" coords="<?php echo $imagemappolygon['coordinates']; ?>" alt="<?php echo $imagemappolygon['name']; ?>" data-over="<?php echo $imagemappolygon['slug']; ?>">
							<?php } ?>
						</map>
					<?php } ?>
					<?php foreach ($imagemappolygons as $imagemappolygon) { ?>
						<img id="overlay_<?php echo $imagemappolygon['slug']; ?>" class="plan_overlay" src="<?php echo $plan_hover_transparent['url']; ?>" data-off="<?php echo $plan_hover_transparent['url']; ?>" data-over="<?php echo $imagemappolygon['hover-image']; ?>">
					<?php } ?>
				</div>
			</div>
			<div id="development-availability">
			<?php // Availability
			$lots = mred_get_lots();
			$floors = mred_get_floors();
			
			$lines = array();
			$buildingImages = array();
			// Collect Data
			foreach ($lots as $lot) {
				$floor = $floors[$lot['floor_id']];
				$floor = mred_get_floor($lot['floor_id']);
				$building = mred_get_building($floor['building_id']);
				
				// reserved | sold | available
				$lines[utf8_decode($building['name'])][$lot['availability']]++;
				// Add 3d image if present
				$buildingImages[utf8_decode($building['name'])] = $building['building_image_3d'];
			}
			
			// Print Results
			$html = '';
			foreach($lines as $buildingName => $aBuilding){
				
				if(array_key_exists('available', $aBuilding)) {$av = $aBuilding['available'];}
				else $av = 0;
				if(array_key_exists('reserved', $aBuilding)) {$re = $aBuilding['reserved'];}
				else $re = 0;
				if(array_key_exists('sold', $aBuilding)) {$so = $aBuilding['sold'];}
				else $so = 0;
				
				$html .= '<div class="availability-building">';
					$html .= '<div class="a-b-img">';
					if(!empty($buildingImages[$buildingName])){$html .= '<img src="'.str_replace('.jpg', '-150x150.jpg', $buildingImages[$buildingName]['url']).'" />';}
					$html .='</div>';
					$html .= '<div class="a-b-txt">';
						$html .= '<div class="a-b-t-title">'.$buildingName.'</div>';
						$html .= '<div class="a-b-t-availability">
									<ul>
										<li class="available"><span class="text">'.mred_get_availability_description('available').'</span>: <span class="color">'.$av.'</span></li>
										<li class="reserved"><span class="text">'.mred_get_availability_description('reserved').'</span>: <span class="color">'.$re.'</span></li>
										<li class="sold"><span class="text">'.mred_get_availability_description('sold').'</span>: <span class="color">'.$so.'</span></li>
									</ul>
								</div>';
					$html .= '</div>';
				$html .= '</div>';
			}
			$html.= '';
			
			echo $html;
			?>
			</div>
			<div class="clearboth"></div>
		</div>

	<div class="clearboth"></div>
	</div>
</div>
<?php get_footer(); ?>