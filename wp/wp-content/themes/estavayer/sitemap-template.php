<?php
/*
Template Name: Sitemap
*/


get_header(); ?>
<div id="theme-page">
	<div class="mk-main-wrapper-holder">
	<div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper full-layout vc_row-fluid mk-grid row-fluid">
		<div class="theme-content" itemprop="mainContentOfPage">
			
			<div class="site-map-left">
				<h4 id="pages"><?php _e("Pages", "mk_framework"); ?></h4>
				<ul>
				<?php
				wp_list_pages(
				  array(
				    'exclude' => '23,15,13,1014',
				    'title_li' => '',
				  )
				);
				?>
				</ul>
			</div>
			
			<div class="site-map-right">
				<h4 id="posts"><?php _e("Articles", "mk_framework"); ?></h4>
			

				<?php
				$cats = get_categories('exclude=');
				foreach ($cats as $cat) {
				  echo "<h5>".$cat->cat_name."</h5>";
				  echo "<ul style='margin-bottom:25px;'>";
				  query_posts('posts_per_page=-1&cat='.$cat->cat_ID);
				  while(have_posts()) {
				    the_post();
				    $category = get_the_category();
				    // Only display a post link once, even if it's in multiple categories
				    if ($category[0]->cat_ID == $cat->cat_ID) {
				      echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
				    }
				  }
				  echo "</ul>";
				}
				?>
			</div>
			<div class="clearboth"></div>
		</div>
	<div class="clearboth"></div>	
	</div>
	<div class="clearboth"></div>
	</div>
</div>
<?php get_footer(); ?>