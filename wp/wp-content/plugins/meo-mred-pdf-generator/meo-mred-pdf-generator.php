<?php
/*
Plugin Name: MMPG - MEO MRED PDF / JS/TXT Files Generator
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch

- Constants
- Dependencies
- PDF Creation
- JS/TXT Creation
- Auto Update Action Hooks
- Common functions


	#######################################################################################
	## /!\ Comment add_action( 'wp_enqueue_scripts', 'mred_localise' );              /!\ ##
	## /!\ in meo-real-estate-developments.php in meo-real-estate-development plugin /!\ ##
	#######################################################################################


*/
#error_reporting(E_ALL & ~E_WARNING & ~E_STRICK);
#ini_set('display_errors', 1);
/* Constants */
	# PDF
	define('MMPG_PDF_FILEPATH', 'habiter-estavayer-liste-de-prix.pdf');
	define('MMPG_PDF_DOCUMENT', 'habiter-estavayer-liste-de-prix.pdf');
	define('MMPG_TEXT_DOMAIN', 'meo-mred-pdf-generator');
	define('MMPG_PDF_DESTINATION', dirname(__FILE__).'/../../uploads/2015/07/');
	define('MMPG_PDF_NAME', 'habiter-estavayer-liste-de-prix.pdf'); // -generated
	define('MMPG_PDF_NAME_C1', 'habiter-estavayer-liste-de-prix-c1.pdf'); // -generated
	define('MMPG_PDF_NAME_C2', 'habiter-estavayer-liste-de-prix-c2.pdf'); // -generated
	define('MMPG_LOT_REF', '3556-'); // Special code for the Lt Ref column, no clue really where it comes from
	# JS/TXT
	define('MMPG_TXT_PATH', dirname(__FILE__).'/../../uploads/cache/');

/* Dependencies */
$required_plugins = array('meo-real-estate-developments/meo-real-estate-developments.php');

function mmpg_plugindependencycheck() {
	global $required_plugins;
	if (empty($required_plugins)) {
		return true;
	}
	if (!function_exists('is_plugin_active')) {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	}
	foreach ($required_plugins as $required_plugin) {
		if (!is_plugin_active($required_plugin)) {
			add_action('admin_notices', 'mmpg_missingplugin');
			return false;
		}
	}
	return true;
}

function mmpg_missingplugin() {
	global $required_plugins;
	echo '<div class="updated fade">' .
		__('Error: plugin "MMPG - MEO MRED PDF Generator" depends on the following" plugins.  ',  MMPG_TEXT_DOMAIN).
		__('Please install and/or activate them', MMPG_TEXT_DOMAIN) .
		'<br/>' . join('<br/>', $required_plugins) .
		'</div>';
}

/* Check dependencies */
$dependencies_present = mmpg_plugindependencycheck();

// If all the dependencies are there, we're good to go
if ($dependencies_present) {
	
	
	##########################################
	##					PDF					##
	##########################################
	
	$plugin_root = plugin_dir_path( __FILE__ );

	require_once( $plugin_root . 'fpdf/fpdf.php' );
	require_once( $plugin_root . 'class.mredtablepdf.php' );

	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('MeoMPDFGen', 'activate'));
	register_deactivation_hook(__FILE__, array('MeoMPDFGen', 'deactivate'));
	
	// Function to collect info and generate the PDF into a file on the server
	function mmpg_fpdf_generate($file_code_sector = ''){

		// Collect Info
		if(function_exists('mred_localise')){
			
			// Force empty ~cache ~data
			mred_reset();
			
			// Get fresh info
			$lots = mred_get_lots();
			$floors = mred_get_floors();
			
			$lines = array();
			
			$index = 1;
			foreach ($lots as $lot) {

				$floor = $floors[$lot['floor_id']];
	
					global $download_lot_id;
					$download_lot_id = $lot['id'];
					
					$floor = mred_get_floor($lot['floor_id']);
					$building = mred_get_building($floor['building_id']);
					
					$parking = get_field( "parking", $lot['id'] );
					if(empty( $parking )) { 
						$parking = '1'; # for 2.5 and 3.5 rooms
					}
					
					if($lot['pieces'] > 3.5){
						$parking = '2'; # for 4.5 and 5.5 rooms
					}
					
					// Floors / Duplex?
					$floor_temp = false;
					if($lot['multifloor']){
						$duplex = " / Dupl.";
						foreach ($lot['all_floors'] as $loop_floor_id => $loop_floor_details) {
							if($loop_floor_details['entry'] == 1) {
								$entry_floor = mred_get_floor($loop_floor_id);
								$floor_temp = $entry_floor['ordinal'];
							}
							
						}
						if(!$floor_temp) {$floor_temp = $floor['ordinal'];}
					}
					else {
						$duplex = "";
						$floor_temp = $floor['ordinal'];
					}
					
					// Floor formatting
					$floor_temp = str_replace(array('<sup>', '</sup>'), '', $floor_temp);
					$floor_temp = utf8_decode($floor_temp);
					$floor_temp = htmlentities($floor_temp);
					$floor_temp = str_replace(array('&amp;egrave;','eme','er','me'), '', $floor_temp);
                    $floor_temp = (empty($floor_temp)) ? '2' : $floor_temp;
					
					if($floor_temp == '3') {$floor_temp = 'Attique';}

                    list($code_sector, $code_building, $code_floor, $code_lot) = explode('.', $lot['code']);

					$line = array();
					$empty = '';
					if($file_code_sector == $code_sector || empty($file_code_sector)) {
                        $line[] = utf8_decode('Étape '.substr($code_sector,1)).' - '.__($lot['name']).''; 						// Reference (MMPG_LOT_REF.''.$index)

                        //$line[] = $lot['code'].''; 						// Reference (MMPG_LOT_REF.''.$index)
                        $line[] = ''.utf8_decode($building['name']).''; 			// Building Name
                        $line[] = ''.utf8_decode($lot['pieces']).$duplex; 			// Nb Rooms / Duplex
                        $line[] = ''.$floor_temp.''; 								// Floor
                        $line[] = ''.$parking.'';									// Parking
                        $line[] = ''.number_format($lot['surface_weighted'], 0).iconv('UTF-8', 'windows-1252', ' m²'); // Surface Pond.
                        $line[] = ''.utf8_decode($lot['availability']).'';			// Price & Status

                        $line[] = number_format((int)$lot['price'], 0, ".", "'"); 	// Will store the price in a separated cell

                        $lines[] = $line;

                        $index++;
                    }
			}

			// Table Version
			$HTML = '<table border="0">';
			
			$i = 0;
			foreach($lines as $l => $lot){
				
				if ($i % 2 == 0) { $lineBG = '#d1d2d4'; }
				else $lineBG = '#ffffff';
				
				// Availability
				if($lot[6] == 'reserved'){
					$lot[6] = 'RESERVE';
					$color = '#f7942a';
				}
				else if($lot[6] == 'sold'){
					$lot[6] = 'VENDU';
					$color = '#ee2e2d';
				}
				// available
				else {
					$price = (!empty($lot[7])) ? $lot[7].' CHF' : 'NA';
					$lot[6] = $price;
					$color = '#000000';
				}
				
				$HTML.= '<tr>
						<td width="110" height="60" bgcolor="'.$lineBG.'">'.$lot[0].'</td>
						<td width="110" height="60" bgcolor="'.$lineBG.'">'.$lot[1].'</td>
						<td width="110" height="60" bgcolor="'.$lineBG.'">'.$lot[2].'</td>
						<td width="110" height="60" bgcolor="'.$lineBG.'">'.$lot[3].'</td>
						<td width="110" height="60" bgcolor="'.$lineBG.'">'.$lot[4].'</td>
						<td width="110" height="60" bgcolor="'.$lineBG.'">'.$lot[5].'</td>
						<td width="110" height="60" bgcolor="'.$lineBG.'"><font color="'.$color.'">'.$lot[6].'</font></td>
					</tr>';
				$i++;
			}
			
			$HTML .= '</table>';
			
			// Prepare PDF
			$pdf = new MredTablePDF();
			$pdf->SetAutoPageBreak(true, 50);
			$pdf->AddPage();
			
			// Content
			$pdf->SetFont('Helvetica','',10);
			$pdf->SetTextColor(0);
			$pdf->WriteHTML($HTML);

            if($file_code_sector == 'C1'){
                return $pdf->Output(MMPG_PDF_DESTINATION.MMPG_PDF_NAME_C1, "F");
            }else if($file_code_sector == 'C2'){
                return $pdf->Output(MMPG_PDF_DESTINATION.MMPG_PDF_NAME_C2, "F");
            }else{
                return $pdf->Output(MMPG_PDF_DESTINATION.MMPG_PDF_NAME, "F");
            }


		}
	}
	/* Endif; function_exists */
	
	##########################################
	##				 JS/TXT					##
	##########################################
	
	function mmpg_generate_scripts_callback($echo = false, $continue = false) {
		global $wpdb; // this is how you get access to the database
		
		// Get all the info from Database
		$dataToJS = array(
			'lots' => mred_get_lots(array('pdf', 'floor_position_image', 'floor_position_image_fallback', 'lot_situation_image', 'lot_situation_image_fallback')),
			'plans' => mred_get_plans(array('pdf')),
			'floors' => mred_get_floors(array('plan')),
			'sectors' => mred_get_sectors(),
			'buildings' => mred_get_buildings(),
			'lot_types' => mred_get_lot_types(),
			'entries' => mred_get_entries(),
			'translations' => array(
				'rooms' => mred_translate('Rooms'),
				'apartment' => mred_translate('Apartment'),
				'statuses' => mred_get_availability_descriptions()
			)
		);
		
		# Update: We need two files now, one with the price, one without
		
		// Turn Data into JS code [Price]
		$scriptPrice = mmpg_dataToJS('mred', $dataToJS, true);
		
			// Remove price
			foreach($dataToJS['lots'] as $k => $lot){
				unset($dataToJS['lots'][$k]['price']);
			}
		
		// Turn Data into JS code [No Price]
		$script = mmpg_dataToJS('mred', $dataToJS, true);
		
		// Get Script file path
		$destPath = realpath(MMPG_TXT_PATH);
		$filePathPrice   = $destPath.'/script-price.txt';
		$filePathNoPrice = $destPath.'/script.txt';
		
		// Fill script files
		$cached = fopen($filePathPrice, 'w');
		fwrite($cached, $scriptPrice);
		fclose($cached);
		
		$cached = fopen($filePathNoPrice, 'w');
		fwrite($cached, $script);
		fclose($cached);
		
		// Echo the content (from header.php if file didn't exist (now it's created btw))
		if($echo){
			// Detect if we print prices or not
			if(!isset($_SESSION['pricesEnabled']) || $_SESSION['pricesEnabled'] == false) {
				echo $script;
			}
			else echo $scriptPrice;
			
			return true;
		}
		else {
			# Commented below as it must not print anything during AJAX call
			// echo 'Fichers crees | Files created | Content cached';
			if($continue) return true; // Stop when updating from admin
			wp_die();
		}
	}
	
	##########################################
	##		Auto Update Action Hooks		##
	##########################################
	
	function mmpg_regeneration(){
		mmpg_fpdf_generate();
        mmpg_fpdf_generate('C1');
        mmpg_fpdf_generate('C2');
        mmpg_generate_scripts_callback(false, true);
	}
	
	// Auto generate in these cases
    /*add_action('delete_post', 'mmpg_regeneration', 9999, 0);
    add_action('publish_post', 'mmpg_regeneration', 9999, 0);*/
	add_action('save_post', 'mmpg_regeneration', 9999, 0);
}

##########################################
##		    Common Functions			##
##########################################

## PDF

	// Convert hex html code (e.g. #3FE5AA) into associative array (keys: R,G,B)
	function hex2dec($couleur = "#000000"){
	    $R = substr($couleur, 1, 2);
	    $rouge = hexdec($R);
	    $V = substr($couleur, 3, 2);
	    $vert = hexdec($V);
	    $B = substr($couleur, 5, 2);
	    $bleu = hexdec($B);
	    $tbl_couleur = array();
	    $tbl_couleur['R']=$rouge;
	    $tbl_couleur['G']=$vert;
	    $tbl_couleur['B']=$bleu;
	    return $tbl_couleur;
	}
	
	//conversion pixel -> millimeter in 72 dpi
	function px2mm($px){
	    return $px*25.4/72;
	}
	
	function txtentities($html){
	    $trans = get_html_translation_table(HTML_ENTITIES);
	    $trans = array_flip($trans);
	    return strtr($html, $trans);
	}

## JS/TXT
	
	/* Emulate Localize to Cache the JS content from Db (from mred_localise() MEO Real Estate Plugin) */

	/**
	 * meo_dataToJS
	 * @param $varName (string): Javascript var name
	 * @param $l10n (string): PHP code to convert
	 * @param $fullscriptEmbed (bool) : boolean to add or not the javascript tags
	 */
	function mmpg_dataToJS( $varName, $l10n, $fullscriptEmbed = false ) {
	
		// back compat, preserve the code in 'l10n_print_after' if present
		if ( is_array($l10n) && isset($l10n['l10n_print_after']) ) { 
			$after = $l10n['l10n_print_after'];
			unset($l10n['l10n_print_after']);
		}
	
		foreach ( (array) $l10n as $key => $value ) {
			if ( !is_scalar($value) )
				continue;
	
			$l10n[$key] = html_entity_decode( (string) $value, ENT_QUOTES, 'UTF-8');
		}
		
		// Return
		$return = '';
		
		if($fullscriptEmbed){$return .= '<script type="text/javascript">/* <![CDATA[ */'."\n";}
		
		$script = "var $varName = " . wp_json_encode( $l10n ) . ';'."\n";
		$return .= $script;
		
		if($fullscriptEmbed){$return .= '/* ]]> */</script>';}
	
		return $return;
	}
	
	# Uncomment below if you want the admin button to generate the file manually
	
	/* Add Btn to Generate Manually the script file */
	/**
	 * This allows to avoid to query multiple times, per page, the database
	 * The file contains the generated code once, and can be updated manually
	 * in the admin section
	 * 
	 * Btn is in the admin footer
	 * Script file is under the wp-content/uploads/cache folder
	 * /
	add_action('admin_footer', 'generate_script_button');
	function generate_script_button() { ?>
		<script type="text/javascript" >
		jQuery(document).ready(function($) {
	
			if(jQuery('#generate-script').length){
				var gs = jQuery('#generate-script');
				gs.click(function(e){
					e.preventDefault();
					var data = {
						'action': 'generate_script'
					};
			
					// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
					$.post(ajaxurl, data, function(response) {
						alert('Resultat: ' + response);
					});
				});
			}
		});
		</script>
		<style>
		<!--
		#script-generation {
		  background: none repeat scroll 0 0 #f7a770;
		  bottom: 0;
		  clear: both;
		  display: block;
		  height: 30px;
		  padding: 5px 0 15px;
		  position: absolute;
		  right: 0;
		  text-align: center;
		  width: 400px;
		}
		#script-generation a {color:#fff;font-weight: bold;}
		-->
		</style>
		<div id='script-generation'>
			<p>
				<a href='#' class='sg' id='generate-script'>R&eacute;g&eacute;n&eacute;rer le fichier Informations (B&acirc;timent, Etage, Lot...)</a>
			</p>
		</div>
		<?php 
	}
	add_action( 'wp_ajax_generate_script', 'mmpg_generate_scripts_callback' );
	/* */