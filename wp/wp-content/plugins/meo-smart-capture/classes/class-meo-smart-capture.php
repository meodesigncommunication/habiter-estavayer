<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoSmartCapture {

	const DB_VERSION = '1.0.0';

	private $cf7integration = null;
	private $utilities = null;
	private $downloadFormUrl = null;

	public function __construct() {
		require_once( dirname(__FILE__) . '/class-meosc-utilities.php');
		$this->utilities = new MeoScCf7Utilities();

		require_once( dirname(__FILE__) . '/class-meosc-cf7-integration.php');
		$this->cf7integration = new MeoScCf7Integration($this, $this->utilities);

		$this->addHooks();
	}

	public static function activate() {
		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$charset_collate = '';

		if ( ! empty( $wpdb->charset ) ) {
			$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		}

		if ( ! empty( $wpdb->collate ) ) {
			$charset_collate .= " COLLATE {$wpdb->collate}";
		}


		$sql = "CREATE TABLE " . $wpdb->prefix . "file_downloader (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			surname varchar(100) NOT NULL,
			first_name varchar(100) NOT NULL,
			email varchar(100) NOT NULL,
			phone varchar(100) NOT NULL,
			address varchar(100) NOT NULL,
			postcode varchar(100) NOT NULL,
			city varchar(100) NOT NULL,
			country varchar(100) NOT NULL,
			language varchar(100) NOT NULL,
			creation_date datetime DEFAULT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY email (email)
		) $charset_collate;";

		dbDelta( $sql );


		$sql = "CREATE TABLE " . $wpdb->prefix . "file_download (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			file_downloader_id bigint(20) unsigned NOT NULL,
			attachment_id bigint(20) unsigned DEFAULT NULL,
			download_count int(11) NOT NULL DEFAULT '0',
			download_code varchar(20) NOT NULL,
			PRIMARY KEY (id),
			KEY file_download_file_downloader_fk (file_downloader_id),
			CONSTRAINT file_download_file_downloader_fk FOREIGN KEY (file_downloader_id) REFERENCES " . $wpdb->prefix . "file_downloader (id) ON UPDATE CASCADE
		) $charset_collate;";

		dbDelta( $sql );


		add_option( 'msc_db_version', self::DB_VERSION );
	}

	public static function deactivate() {
		// Do nothing - leave the tables in case the plugin's reactivated
	}

	public function addHooks() {
		$this->cf7integration->addHooks();
		add_filter('attachment_link', array($this, 'convertPdfLinks'), 10, 2);
		add_shortcode('attachment_download_link', array($this, 'generateDownloadLink'));
	}

	public function convertPdfLinks($link, $post_id) {
		global  $download_lot_id, $post, $meoMIMEtypes;

		$attachment = get_post( $post_id );
		
		// Allowed MIME TYPE (PDF, XLS)
		
		// if ($attachment->post_mime_type == 'application/pdf') {
		if (in_array($attachment->post_mime_type, $meoMIMEtypes)) {
			$download_form_url = $this->getDownloadFormUrl();
			if (!empty($download_form_url)) {
				$result = $download_form_url . '?file_id=' . $this->utilities->encodeAttachmentId($post_id) . '&post_id=' . ( empty($download_lot_id) ? $post->ID : $download_lot_id);
				return $result;
			}
		}

		return $link;
	}

	private function getDownloadFormUrl() {
		if (!empty($this->downloadFormUrl)) {
			return $this->downloadFormUrl;
		}

		$download_page_id = $this->utilities->getPostIdForTemplate('../templates/msc-attachment-request.php');

		if (empty($download_page_id)) {
			return null;
		}

		$this->downloadFormUrl = get_permalink($download_page_id);
		return $this->downloadFormUrl;
	}

	public function getDownloadLink($cf7, $posted_data, $file_id) {
		$downloader_id = $this->getDownloaderId($cf7, $posted_data);
		$download_code = $this->getDownloadCode($downloader_id, $file_id);

		$download_link_id = $this->utilities->getPostIdForTemplate('../templates/msc-attachment-download-handler.php');
		$download_url = get_permalink( $download_link_id ) . '?email=' . urlencode($posted_data['email']) . '&download_code=' . urlencode($download_code);

		return $download_url;
	}

	private function getDownloaderId($cf7, $posted_data) {
		global $wpdb;

		$downloader_id = $wpdb->get_var( $wpdb->prepare(
			"select pd.id
			   from {$wpdb->prefix}file_downloader pd
			  where pd.email = %s",
			$posted_data['email']
		) );

		if (empty($downloader_id)) {
			$values = array(
				'surname'       => $posted_data['surname'],
				'first_name'    => $posted_data['first_name'],
				'email'         => $posted_data['email'],
				'phone'         => $posted_data['phone'],
				'address'       => $posted_data['address'],
				'postcode'      => $posted_data['postcode'],
				'city'          => $posted_data['city'],
				'country'       => $posted_data['country'],
				'language'      => substr($posted_data['_wpcf7_locale'], 0, 2),
				'creation_date' => current_time('mysql', 1)
			);

			$placeholders = array(
				'%s', // surname
				'%s', // first_name
				'%s', // email
				'%s', // phone
				'%s', // address
				'%s', // postcode
				'%s', // city
				'%s', // country
				'%s', // language
				'%s'  // creation_date
			);

			$wpdb->insert(
				"{$wpdb->prefix}file_downloader",
				$values,
				$placeholders
			);

			$downloader_id = $wpdb->insert_id;

			$this->schedulePdfEmail($cf7, $posted_data);
		}
		else {
			// Don't send the form content to the salespeople if they know about this contact
			$cf7->skip_mail = true;
		}

		return $downloader_id;
	}

	// Tell the SmartCapture admin to send the salesman a PDF for this visitor
	private function schedulePdfEmail($cf7, $posted_data) {
		$smart_capture_url = get_field('smart_capture_url', 'option');
		$api_key = get_field('api_key', 'option');
		if (empty($smart_capture_url) || empty($api_key)) {
			// Will send the normal cf7 email
			return;
		}

		$analytics_id = $posted_data['analytics_id'];
		if (empty($analytics_id)) {
			require_once( dirname(__FILE__) . '/class-meosc-piwik-integration.php');
			$analytics_id = MeoPiwikIntegration::mscPiwikGetAnalyticsIdFromCookie();
		}

		if (empty($analytics_id)) {
			require_once( dirname(__FILE__) . '/class-meo-error-handler.php');
			MeoErrorHandler::reportError(__FILE__, __LINE__, array(
				'message'      => "Analytics ID missing",
				'$cf7'         => $cf7,
				'$posted_data' => $posted_data
			));
			return;
		}

		$schedule_url = $smart_capture_url . "/wp/wp-admin/admin-ajax.php?action=schedule_pdf_email&analytics_id=" . $analytics_id . "&api_key=" . $api_key;

		$curl = new WP_Http_Curl();
		$response = $curl->request($schedule_url, array(
			'headers' => array(
				'Accept'       => 'application/json',
				'Content-type' => 'application/json',
			),
			'method' => 'GET',
			'redirection' => 0
		));

		if ( is_wp_error( $response ) || empty( $response['body']) ) {
			// The call to smart capture failed.  Fall back to the normal email
			return;
		}

		$schedule_data = json_decode($response['body']);

		if ($schedule_data->success) {
			// Don't send the cf7 email - the salespeople will get the PDF
			// Commented below [temporary] as we want to sent the c7 form with the new extra fields.
			// $cf7->skip_mail = true;
		}

		// The call to smart capture failed.  Fall back to the normal email
	}

	private function getDownloadCode($downloader_id, $file_id) {
		global $wpdb;
		$download_code = $wpdb->get_var('SELECT UUID_SHORT() as download_code');
		if (empty($download_code)) {
			$download_code = substr(str_shuffle("012345678901234567890123456789012345678901234567890123456789"), 0, 17);
		}

		$result = $wpdb->insert
		(
			"{$wpdb->prefix}file_download",
			array(
				'file_downloader_id' => $downloader_id,
				'attachment_id'      => $file_id,
				'download_code'      => $download_code
			),
			array(
				'%d',
				'%d',
				'%s'
			)
		);

		if (false === $result) {
			return null;
		}

		return $download_code;
	}

	public function generateDownloadLink( $atts, $content = null ) {
		global $post;

		$atts = shortcode_atts( array(
			'attachment_id' => null,
			'class'         => null
		), $atts );

		$content = do_shortcode($content);
		$attachment_id = (int) $atts['attachment_id'];

		$link = '#';
		if (!empty($attachment_id)) {
			$link = $this->getDownloadFormUrl();
			$link .= '?file_id=' . $this->utilities->encodeAttachmentId( $attachment_id ) . '&post_id=' . $post->ID;

		}

		return '<a href="' . $link . '"' . ( $atts['class'] ? ' class = "' . $atts['class'] . '"' : '' ) . '>' . $content . '</a>';
	}

	public function decodeAttachmentId($obfuscated_id) {
		return $this->utilities->decodeAttachmentId($obfuscated_id);
	}

	public function createConfigPage() {
		if(function_exists("register_field_group"))
		{

			$page_stub = defined('ACF_OPTION_PAGE_STUB') ? ACF_OPTION_PAGE_STUB : 'acf-options';

			register_field_group(array (
				'id' => 'acf_smart-capture-config',
				'title' => 'Smart Capture config',
				'fields' => array (
					array (
						'key' => 'field_54048ccc0c678',
						'label' => 'Smart Capture API key',
						'name' => 'api_key',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5404937fb75a3',
						'label' => 'piwik site id',
						'name' => 'piwik_site_id',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5413ff7f0be89',
						'label' => 'Smart Capture URL',
						'name' => 'smart_capture_url',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => $page_stub,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function addPiwikCode() {
		if (function_exists('mred_get_piwik_code')) {
			mred_get_piwik_code(defined('IS_PRODUCTION') ? IS_PRODUCTION : false);
		}
	}
}
