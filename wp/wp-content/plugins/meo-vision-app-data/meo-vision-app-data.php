<?php
/*
Plugin Name: MEO Vision App Data
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/

--------------------------------------------------------------------------

Copyright (C) MEO design et communication S�rl 2015. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited.
Proprietary and confidential

info@meomeo.ch

--------------------------------------------------------------------------

Provides an ajax interface for the MEO vision app into the MEO real
estate development (MRED) data.  Requires that the MRED plugin is already
activated.

Code is similar to the MRED DAO, but performance tuned for the subset
of data required by the app.

*/

if (!defined('MVAD_TEXT_DOMAIN')) {
	define('MVAD_TEXT_DOMAIN', 'meo_vision_app_data');
}

if (!defined('PLAN_IMAGE_SIZE_LARGE')) {
	define('PLAN_IMAGE_SIZE_LARGE', 'meo-vision-app-plan-large');
}

if (!defined('PLAN_IMAGE_SIZE_THUMB')) {
	define('PLAN_IMAGE_SIZE_THUMB', 'meo-vision-app-plan-thumb');
}

global $required_plugins;
$required_plugins = array('meo-real-estate-developments/meo-real-estate-developments.php');

function mvad_missingplugin() {
	global $required_plugins;
	echo '<div class="updated fade">' .
		__('Error: plugin "MEO Vision App Data" depends on the following" plugins.  ',  MVAD_TEXT_DOMAIN).
		__('Please install and/or activate them', MVAD_TEXT_DOMAIN) .
		'<br/>' . join('<br/>', $required_plugins) .
		'</div>';
}

function mvap_plugindependencycheck() {
	global $required_plugins;
	if (empty($required_plugins)) {
		return true;
	}
	if (!function_exists('is_plugin_active')) {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	}
	foreach ($required_plugins as $required_plugin) {
		if (!is_plugin_active($required_plugin)) {
			add_action('admin_notices', 'mvap_missingplugin');
			return false;
		}
	}
	return true;
}

$dependencies_present = mvap_plugindependencycheck();

if ($dependencies_present) {
	global $mvad;

	$plugin_dir = dirname(plugin_basename(__FILE__));
	load_plugin_textdomain(MVAD_TEXT_DOMAIN, false, $plugin_dir . '/languages/');

	$plugin_root = plugin_dir_path( __FILE__ );

	require_once( $plugin_root . 'classes/class-meo-vision-app-data.php');

	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('MeoVisionAppData', 'activate'));
	register_deactivation_hook(__FILE__, array('MeoVisionAppData', 'deactivate'));

	$mvad = new MeoVisionAppData();

	add_action('wp_ajax_get_app_development_data', array($mvad, 'getAppDevelopmentData'));
	add_action('wp_ajax_nopriv_get_app_development_data', array($mvad, 'getAppDevelopmentData'));

	add_image_size( PLAN_IMAGE_SIZE_LARGE, 2516, 2500, array( 'center', 'center' ) );
	add_image_size( PLAN_IMAGE_SIZE_THUMB,  216,  214, array( 'center', 'center' ) );
}
