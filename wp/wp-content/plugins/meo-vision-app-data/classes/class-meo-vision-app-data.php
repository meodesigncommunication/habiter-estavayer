<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoVisionAppData {
	const CPT_FEATURES      = 'features';
	const REL_PLAN_FEATURES = 'plans_to_features';

	public function __construct() {
		if (!function_exists('is_plugin_active')) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		add_action('init', array(&$this, 'createCustomPostTypes'), 5);
		add_action('init', array(&$this, 'createFieldForExistingCustomPostTypes'), 5);
		add_action('p2p_init', array(&$this, 'createRelationships'));

		add_action('after_setup_theme', array(&$this, 'createConfigPage'), 50); // Should run after smart capture plugin's createConfigPage(), if present, as it's only needed if the API key field isn't defined
	}


	public static function activate() {
		// Do nothing
	}

	public static function deactivate() {
		// Do nothing
	}

	public function getAppDevelopmentData() {
		header('Content-Type: application/json');
		$this->checkApiKey();

		$result = array(
			'lots' => $this->getLots(),
			'plans' => $this->getPlans(),
			'floors' => $this->getFloors(),
			'buildings' => $this->getBuildings(),
			'entries' =>  $this->getEntries(),
			'features' =>  $this->getFeatures()
		);

		echo json_encode($result);
		exit;

	}

	public function createCustomPostTypes() {
		register_post_type(self::CPT_FEATURES, array(
			'labels' => array(
				'name' => __('Features', MVAD_TEXT_DOMAIN),
				'singular_name' => __('Feature', MVAD_TEXT_DOMAIN),
				'add_new_item' => __('Add an feature', MVAD_TEXT_DOMAIN),
				'edit_item' => __('Edit feature', MVAD_TEXT_DOMAIN),
				'new_item' => __('Add an feature', MVAD_TEXT_DOMAIN),
				'search_items' => __('Find an feature', MVAD_TEXT_DOMAIN),
				'not_found' => __('No feature found', MVAD_TEXT_DOMAIN),
				'not_found_in_trash' => __('No feature found in the trash', MVAD_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => array('title', 'editor')
		));

	}


	public function createFieldForExistingCustomPostTypes() {
		if( function_exists('register_field_group') ) {

			register_field_group(array (
				'key' => 'group_558d465ec45ec',
				'title' => 'App plan fields',
				'fields' => $this->getAppPlanFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => DevelopmentDaoP2P::CPT_PLAN,
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
			));

			register_field_group(array (
				'key' => 'group_558d488c9d051',
				'title' => 'App lot fields',
				'fields' =>$this->getAppLotFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => DevelopmentDaoP2P::CPT_LOT,
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
			));

		}
	}

	public function createRelationships() {
		if (! function_exists('p2p_register_connection_type')) {
			error_log("p2p_register_connection_type function doesn't exist");
			return;
		}

		$relationships = array(
			self::REL_PLAN_FEATURES => array(
				'from' => DevelopmentDaoP2P::CPT_PLAN,
				'to' => self::CPT_FEATURES
			)
		);

		foreach ($relationships as $relationship_name => $relationship_details) {
			p2p_register_connection_type( array(
				'name' => $relationship_name,
				'from' => $relationship_details['from'],
				'to' => $relationship_details['to'],
				'admin_column' => 'from',
				'cardinality' => 'many-to-one'
			) );
		}
	}

	public function createConfigPage() {
		if(!function_exists("register_field_group")) {
			return;
		}

		if ($this->acfOptionsFieldExists( 'api_key' )) {
			return;
		}

		if (empty($GLOBALS['acf_options_page'])) {
			acf_add_options_page('MEO App Settings');
		}

		$page_slug = $GLOBALS['acf_options_page']->settings['slug'];

		register_field_group(array (
			'id' => 'acf_smart-capture-config',
			'title' => 'Smart Capture config',
			'fields' => array (
				array (
					'key' => 'field_54048ccc0c678',
					'label' => 'Smart Capture API key',
					'name' => 'api_key',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => $page_slug,
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}

	private function acfOptionsFieldExists($field_name) {
		if( !empty($GLOBALS['acf_register_field_group']) ) {
			foreach( $GLOBALS['acf_register_field_group'] as $acf )
			{
				$is_options_page = !empty($acf['location']) && !empty($acf['location'][0]) && !empty($acf['location'][0][0]) && !empty($acf['location'][0][0]['param']) && $acf['location'][0][0]['param'] == 'options_page';
				if( $is_options_page && !empty($acf['fields']) )
				{
					foreach( $acf['fields'] as $f )
					{
						if( $f['name'] == $field_name )
						{
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	private function getAppPlanFieldDefinitions() {
		return array (
					array (
						'key' => 'field_558d46712e63f',
						'label' => 'Floor synoptic (for app)',
						'name' => 'floor_synoptic',
						'prefix' => '',
						'type' => 'image',
						'instructions' => 'Should be 500px wide',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
					),
					array (
						'key' => 'field_558d469d2e640',
						'label' => 'Floor synoptic coordinates',
						'name' => 'floor_synoptic_coordinates',
						'prefix' => '',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
				);
	}

	private function getAppLotFieldDefinitions() {
		return array (
					array (
						'key' => 'field_558d4898cde6f',
						'label' => 'Floor synoptic (for app)',
						'name' => 'floor_synoptic',
						'prefix' => '',
						'type' => 'image',
						'instructions' => 'Should be 500px wide',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
					),
					array (
						'key' => 'field_558d48bacde70',
						'label' => 'Floor synoptic coordinates',
						'name' => 'floor_synoptic_coordinates',
						'prefix' => '',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
				);
	}

	private function checkApiKey() {
		$local_key = get_field('api_key', 'option');

		if ($_REQUEST['api-key'] != $local_key) {
			echo json_encode(array('error'));
			exit;
		}
	}

	private function getLots() {
		global $wpdb;

		$sql = "  select l2f.p2p_from as lot_id
				       , l2f.p2p_to as floor_id
				    from " . $wpdb->prefix . "p2p l2f
				         left outer join " . $wpdb->prefix . "p2pmeta l2fm on l2f.p2p_id = l2fm.p2p_id and l2fm.meta_key = '_order_from'
				   where l2f.p2p_type = '" . DevelopmentDaoP2P::REL_LOT_FLOOR . "'
				order by l2f.p2p_from
				       , l2fm.meta_value
				       , l2f.p2p_to ";

		$floors_for_lot = array();

		$rows = $wpdb->get_results($sql);
		foreach ($rows as $row) {
			if (!array_key_exists($row->lot_id, $floors_for_lot)) {
				$floors_for_lot[$row->lot_id] = array();
			}
			$floors_for_lot[$row->lot_id][] = $row->floor_id;
		}


		$sql = "select l.id as id
				     , l.post_title as name
				     , l2f.p2p_to as floor_id
				     , l2p.p2p_to as plan_id
				     , l2e.p2p_to as entry_id
				     , si.meta_value as surface_interior
				     , sb.meta_value as surface_balcony
				     , st.meta_value as surface_terrace
				     , sg.meta_value as surface_garden
				     , sw.meta_value as surface_weighted
				     , a.meta_value  as availability
				     , p.meta_value  as pieces
				     , c.meta_value  as code
				     , coalesce(nullif(lk.meta_value, ''), pk.meta_value)  as keyplan
				  from " . $wpdb->prefix . "posts l
				       left join " . $wpdb->prefix . "p2p l2f on l.id = l2f.p2p_from and l2f.p2p_type = '" . DevelopmentDaoP2P::REL_LOT_FLOOR . "'
				       left join " . $wpdb->prefix . "p2p l2p on l.id = l2p.p2p_from and l2p.p2p_type = '" . DevelopmentDaoP2P::REL_LOT_PLAN  . "'
				       left join " . $wpdb->prefix . "p2p l2e on l.id = l2e.p2p_from and l2e.p2p_type = '" . DevelopmentDaoP2P::REL_LOT_ENTRY . "'
				       left join " . $wpdb->prefix . "postmeta si on l.id = si.post_id and si.meta_key = 'surface_interior'
				       left join " . $wpdb->prefix . "postmeta sb on l.id = sb.post_id and sb.meta_key = 'surface_balcony'
				       left join " . $wpdb->prefix . "postmeta st on l.id = st.post_id and st.meta_key = 'surface_terrace'
				       left join " . $wpdb->prefix . "postmeta sg on l.id = sg.post_id and sg.meta_key = 'surface_garden'
				       left join " . $wpdb->prefix . "postmeta sw on l.id = sw.post_id and sw.meta_key = 'surface_weighted'
				       left join " . $wpdb->prefix . "postmeta a  on l.id = a.post_id  and a.meta_key  = 'availability'
				       left join " . $wpdb->prefix . "postmeta p  on l.id = p.post_id  and p.meta_key  = 'pieces'
				       left join " . $wpdb->prefix . "postmeta c  on l.id = c.post_id  and c.meta_key  = 'code'
				       left join " . $wpdb->prefix . "postmeta lk on l.id = lk.post_id and lk.meta_key = 'floor_synoptic'
				       left join " . $wpdb->prefix . "postmeta pk on l2p.p2p_to = pk.post_id and pk.meta_key = 'floor_synoptic'
				 where l.post_type = '" . DevelopmentDaoP2P::CPT_LOT . "'
				   and l.post_status = 'publish'
				   and not exists (
                           select 1
                             from " . $wpdb->prefix . "p2p ent
                                  join " . $wpdb->prefix . "p2pmeta l2fm on ent.p2p_id = l2fm.p2p_id
                            where ent.p2p_type = '" . DevelopmentDaoP2P::REL_LOT_FLOOR . "'
                              and ent.p2p_from = l.id -- entry is for same lot
                              and ent.p2p_to != l2f.p2p_to -- but on a different floor
                              and l2fm.meta_key = 'entry'
                              and ifnull(l2fm.meta_value, '0') = '1')";

		$rows = $wpdb->get_results($sql);

		$result = array();

		foreach ($rows as $row) {
			$lot = array(
				'id' => $row->id,
				'name' => __($row->name),
				'floor_id' => $row->floor_id,
				'plan_id' => $row->plan_id,
				'entry_id' => $row->entry_id,
				'surface_interior' => $row->surface_interior,
				'surface_balcony' => $row->surface_balcony,
				'surface_terrace' => $row->surface_terrace,
				'surface_garden' => $row->surface_garden,
				'surface_weighted' => $row->surface_weighted,
				'availability' => $row->availability,
				'pieces' => $row->pieces,
				'code' => __($row->code),
				'keyplan_image' => array(),
				'all_floors' => array_key_exists($row->id, $floors_for_lot) ? $floors_for_lot[$row->id] : array()
			);

			if ($row->keyplan) {
				$lot['keyplan_image'] = $this->extractMediaDetails($row->keyplan);
			}

			$result[$row->id] = $lot;
		}

		return $result;
	}

	private function getPlans() {
		global $wpdb, $mvas;

		$spinners_active = ( class_exists('MeoVisionAppSpinner', false) && !empty($mvas) );

		$sql = "select p.id as id
				     , p.post_title  as name
				     , c.meta_value  as code
				     , p2f.p2p_to    as feature_id
				     , coalesce(det_im.attachment_id, det_fb.attachment_id) as detailed_plan_image
				     , i3.meta_value as image_3d
				     , " . ( $spinners_active ? "s.p2p_from" : "null" ) . " as spinner_id
				  from " . $wpdb->prefix . "posts p
				       left join " . $wpdb->prefix . "p2p p2f on p.id = p2f.p2p_from and p2f.p2p_type = '" . self::REL_PLAN_FEATURES . "'
				       left join " . $wpdb->prefix . "postmeta c  on p.id = c.post_id  and c.meta_key  = 'code'
				       left join (
				           select di.post_id
				                , a.post_id as attachment_id
				             from " . $wpdb->prefix . "postmeta di
				                  join " . $wpdb->prefix . "postmeta a on di.meta_value = a.post_id
				            where di.meta_key = 'detailed_plan_image'
				              and a.meta_key = '_wp_attached_file'
				              and a.meta_value not like '%.svg'
				       ) det_im on p.id = det_im.post_id
				       left join (
				           select df.post_id
				                , a.post_id as attachment_id
				             from " . $wpdb->prefix . "postmeta df
				                  join " . $wpdb->prefix . "postmeta a on df.meta_value = a.post_id
				            where df.meta_key = 'detailed_plan_image_fallback'
				              and a.meta_key = '_wp_attached_file'
				              and a.meta_value not like '%.svg'
				       ) det_fb on p.id = det_fb.post_id
				       left join " . $wpdb->prefix . "postmeta i3 on p.id = i3.post_id and i3.meta_key = 'image_3d'
				       " . ( $spinners_active ? "left join " . $wpdb->prefix . "p2p s on p.id = s.p2p_to and s.p2p_type = '" . MeoVisionAppSpinner::REL_SPINNER_PLAN . "'" : "" ) . "
				 where p.post_type = '" . DevelopmentDaoP2P::CPT_PLAN . "'
				   and p.post_status = 'publish' 
				 		";

		$rows = $wpdb->get_results($sql);

		$result = array();

		foreach ($rows as $row) {
			$plan = array(
				'id'   => $row->id,
				'name' => __($row->name),
				'code' => __($row->code)
			);

			if ($row->feature_id) {
				$plan['feature_id'] = $row->feature_id;
			}

			if ($row->detailed_plan_image) {
				$plan['detailed_plan_image']  = $this->extractMediaDetails( $row->detailed_plan_image, PLAN_IMAGE_SIZE_LARGE );
				$plan['thumbnail_plan_image'] = $this->extractMediaDetails( $row->detailed_plan_image, PLAN_IMAGE_SIZE_THUMB );
			}

			if ($row->image_3d) {
				$plan['image_3d'] = $this->extractMediaDetails( $row->image_3d, PLAN_IMAGE_SIZE_LARGE );
			}

			if ($row->spinner_id) {
				$plan['spinner_3d'] = $mvas->getSpinner($row->spinner_id);
			}

			$result[$row->id] = $plan;
		}

		return $result;
	}

	private function getFloors() {
		global $wpdb;

		$sql = "select f.id as floor_id,
			           l2f.p2p_from as lot_id,
			           coalesce(nullif(l2fm.meta_value, ''), nullif(lc.meta_value, ''), pc.meta_value)  as floor_synoptic_coordinates
			      from " . $wpdb->prefix . "posts f
			           join " . $wpdb->prefix . "p2p l2f on l2f.p2p_to = f.id and l2f.p2p_type = '" . DevelopmentDaoP2P::REL_LOT_FLOOR . "'
			           left join " . $wpdb->prefix . "p2pmeta l2fm on l2f.p2p_id = l2fm.p2p_id and l2fm.meta_key = 'floor_synoptic_coordinates'
			           join " . $wpdb->prefix . "p2p l2p on l2p.p2p_from = l2f.p2p_from and l2p.p2p_type = '" . DevelopmentDaoP2P::REL_LOT_PLAN . "'
			           left join " . $wpdb->prefix . "postmeta lc on l2f.p2p_from = lc.post_id and lc.meta_key = 'floor_synoptic_coordinates'
			           left join " . $wpdb->prefix . "postmeta pc on l2p.p2p_to = pc.post_id and pc.meta_key = 'floor_synoptic_coordinates'
			     where f.post_type = '" . DevelopmentDaoP2P::CPT_FLOOR . "'
			  order by f.id,
			           l2f.p2p_from";

		$keyplan_lot_coords = array();

		$rows = $wpdb->get_results($sql);
		foreach ($rows as $row) {
			if (!array_key_exists($row->floor_id, $keyplan_lot_coords)) {
				$keyplan_lot_coords[$row->floor_id] = array();
			}
			$keyplan_lot_coords[$row->floor_id][$row->lot_id] = $row->floor_synoptic_coordinates;
		}

		$sql = "select f.id as id
				     , f.post_title as name
				     , c.meta_value as code
				     , o.meta_value as ordinal
				     , ord.meta_value as ord
				     , f2b.p2p_to as building_id
				  from " . $wpdb->prefix . "posts f
				       left join " . $wpdb->prefix . "p2p f2b on f.id = f2b.p2p_from and f2b.p2p_type = '" . DevelopmentDaoP2P::REL_FLOOR_BUILDING . "'
				       left join " . $wpdb->prefix . "postmeta c   on f.id = c.post_id   and c.meta_key   = 'code'
				       left join " . $wpdb->prefix . "postmeta o   on f.id = o.post_id   and o.meta_key   = 'ordinal'
				       left join " . $wpdb->prefix . "postmeta ord on f.id = ord.post_id and ord.meta_key = 'order'
				 where f.post_type = '" . DevelopmentDaoP2P::CPT_FLOOR . "'
				   and f.post_status = 'publish'";

		$rows = $wpdb->get_results($sql);

		$result = array();

		foreach ($rows as $row) {
			$floor = array(
				'id'      => $row->id,
				'name'    => __($row->name),
				'code'    => __($row->code),
				'ordinal' => __($row->ordinal),
				'order'   => __($row->ord),
				'building_id' => $row->building_id
			);

			if (!array_key_exists($row->id, $keyplan_lot_coords)) {
				$keyplan_lot_coords[$row->id] = array();
			}

			$floor['keyplan_lot_coords'] = $keyplan_lot_coords[$row->id];

			$result[$row->id] = $floor;
		}

		return $result;
	}

	private function getBuildings() {
		global $wpdb;

		$sql = "select b.id as id
				     , b.post_title as name
				     , c.meta_value  as code
				  from " . $wpdb->prefix . "posts b
				       left join " . $wpdb->prefix . "postmeta c  on b.id = c.post_id  and c.meta_key  = 'code'
				 where b.post_type = '" . DevelopmentDaoP2P::CPT_BUILDING . "'
				   and b.post_status = 'publish'";

		$rows = $wpdb->get_results($sql);

		$result = array();

		foreach ($rows as $row) {
			$building = array(
				'id'   => $row->id,
				'name' => __($row->name),
				'code' => __($row->code)
			);

			$result[$row->id] = $building;
		}

		return $result;
	}

	private function getEntries() {
		global $wpdb;

		$sql = "select e.id as id
				     , e.post_title as name
				     , c.meta_value as code
				  from " . $wpdb->prefix . "posts e
				       left join " . $wpdb->prefix . "postmeta c  on e.id = c.post_id  and c.meta_key  = 'code'
				 where e.post_type = '" . DevelopmentDaoP2P::CPT_ENTRY . "'
				   and e.post_status = 'publish'";

		$rows = $wpdb->get_results($sql);

		$result = array();

		foreach ($rows as $row) {
			$entry = array(
				'id'   => $row->id,
				'name' => __($row->name),
				'code' => __($row->code)
			);

			$result[$row->id] = $entry;
		}

		return $result;
	}

	private function getFeatures() {
		global $wpdb;

		$sql = "select f.id as id
				     , f.post_title as name
				     , f.post_content as content
				  from " . $wpdb->prefix . "posts f
				 where f.post_type = '" . self::CPT_FEATURES . "'
				   and f.post_status = 'publish'";

		$rows = $wpdb->get_results($sql);

		$result = array();

		foreach ($rows as $row) {
			$entry = array(
				'id'   => $row->id,
				'name' => __($row->name),
				'content' => __($row->content)
			);

			$result[$row->id] = $entry;
		}

		return $result;
	}

	private function extractMediaDetails($id, $size = 'full') {
		$result = null;
		$details = wp_get_attachment_image_src( $id, $size );

		if ($details) {
			$result = array(
				'id'        => $id,
				'url'       => $details[0],
				'mime_type' => get_post_mime_type( $id ),
				'width'     => $details[1],
				'height'    => $details[2]
			);
		}

		return $result;
	}
}
