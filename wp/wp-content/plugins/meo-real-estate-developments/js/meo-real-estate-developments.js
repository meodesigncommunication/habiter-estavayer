var minimumSuperfishFilterWidth = 600,
    superfishFilterActive = false,
    circularNavigation = true,
    trackoutboundLinks = true,
    showAppartmentDetailPopupOnMobile = false,
    isMobile = navigator.userAgent.match(/iPhone|iPad|iPod|Android|BlackBerry|IEMobile/i) ? true : false; // not perfect, but will get the important cases

if (typeof console == "undefined") {
	this.console = {log: function() {}};
}

function track(value, label) {
	if (typeof ga !== "undefined") {
		// Universal Google Analytics
		ga('send', 'pageview', {
			'page': value,
			'title': label ? label : value
		});
	}
	if (typeof _gaq !== "undefined") {
		// Legacy Google Analytics
		_gaq.push(['_trackPageview', value]);
	}
	if (typeof _paq !== "undefined") {
		// Piwik
		if (label) {
			_paq.push(['setDocumentTitle', label]);
		}
		if (typeof post_id !== "undefined") {
			_paq.push(['setCustomVariable', '1', 'post_id',   analytics_post_id,   "page"]);
			_paq.push(['setCustomVariable', '2', 'post_type', analytics_post_type, "page"]);
		}
		_paq.push(['setCustomUrl', value]);
		_paq.push(['trackPageView']);
	}
	console.log(value + " [" + label + "]");
}

function loadLot(lot_id) {
	jQuery('html,body').css('cursor','wait');
	window.location.href = mred.lots[lot_id]['url'];
}

(function($){
	var floor_plan_height;

	function mred_track_outbound_clicks() {
		if (!trackoutboundLinks) {
			return;
		}
		$("a[href^=http]").on('click',function(e){
			var url = $(this).attr("href");
			if (e.currentTarget.host != window.location.host) {
				track(url, "outbound");
				var newtab = (e.metaKey || e.ctrlKey || this.target == "_blank");
				if (!newtab) {
					e.preventDefault();
					setTimeout('document.location = "' + url + '"', 100);
				}
			}
		});
	}

	function mred_add_sorting_icons() {
		$('head').append('<style>.fa-sort, .headerSortDown .fa-sort-up, .headerSortUp .fa-sort-down { display: inline-block; } .fa-sort-up, .fa-sort-down, .headerSortDown .fa-sort, .headerSortUp .fa-sort{ display: none; }</style>');
		$('#apartment-list table thead th').append('&nbsp;&nbsp;<i class="sort-icon fa fa-sort"></i><i class="sort-icon fa fa-sort-up"></i><i class="sort-icon fa fa-sort-down"></i>');
	}

	function mred_add_apartment_list_sort() {
		if (typeof $.fn.tablesorter !== 'function') {
			return;
		}

		// add tablesorter parser for the lot number column
		$.tablesorter.addParser({
			id: 'lots',
			is: function(s) {
				return false; // so parser is not auto detected
			},
			format: function(s) {
				var parts = s.split('-');
				if (parts.length != 2) {
					return s;
				}
				return parts[0] + "-" + ("0000" + parts[1]).slice(-4);
			},
			type: 'text'
		});

		// add tablesorter parser for the floor column
		$.tablesorter.addParser({
			id: 'floor',
			is: function(s) {
				return false; // so parser is not auto detected
			},
			format: function(s) {
				if ( s.toLowerCase() == "rdc" || s.toLowerCase() == "g" ) {
					return 0;
				}
				return parseInt(s, 10);
			},
			type: 'numeric'
		});

		$("#apartment-list table").tablesorter({
			headers: {
				1: { sorter:'floor' },
				2: { sorter:'lots' },
				4: { sorter:'digit' }
			}
		});

		mred_add_sorting_icons();
	}

	function mred_add_apartment_list_switch() {
		var $switchers = $('.view-switcher.in-page');

		if ($switchers.length === 0) {
			return;
		}

		$switchers.click(function(e) {
			e.preventDefault();

			var tab = $('.view-switcher.active').data('analytics-id'),
			    target = $(this).attr('href');

			if ($(target).length === 0) {
				return;
			}

			$('.view-switcher').removeClass('active');
			$(this).addClass('active');

			$('.views .view').hide();
			$(target).show();


			if (typeof track !== 'undefined') {
				track('/plans-par-liste/' + tab);
			}

		});
	}

	// Internal page links in menus need to switch views where appropriate
	function mred_add_menu_switchers() {
		var $switchers = $('.view-switchers'),
		    $menu_switchers = $('.menu-switcher');

		if ($switchers.length === 0) {
			return;
		}

		$menu_switchers.click(function(e){
			var target = $(this).find('a').attr('href'),
			    switcher = $switchers.find('a[href=' + target + ']');

			if (switcher.length) {
				e.preventDefault();
				switcher.click();
			}
		});
	}

	function mred_add_floor_navigation() {

		if ($('.plans-by-floor').length === 0) {
			return;
		}

		$('.floor_link').click(function(e) {
			e.preventDefault();
			var floor_id = $(this).data('floor-id');
			if (typeof $.fn.scroll_to_floor_id !== 'undefined') {
				$.fn.scroll_to_floor_id(floor_id);
			}
		});

		var $floor_name = $('.floor_name'),
			$scroll_arrow_up = $('.floor_navigation_arrow_up'),
			$scroll_arrow_down = $('.floor_navigation_arrow_down'),
			$scroll_container = $('.scroll-container'),
			$building_wrapper = $('.building-wrapper'),
			$floor_plan_wrapper = $('.plans-by-floor .floor-plan-wrapper');

		var $first_floor = $scroll_container.find('.floor-image-wrapper').first(),
			num_floors = $scroll_container.find('.floor-image-wrapper').length;

		function update_floor_label_and_button_states() {
			var floor_id = floor_ids[floor_index],
			    floor_name = mred.floors[floor_id].ordinal;

			$floor_name.html(floor_name);

			$scroll_arrow_up.filter(':visible').removeClass("active disabled").addClass((circularNavigation || floor_index > 0) ? "active" : "disabled");
			$scroll_arrow_down.filter(':visible').removeClass("active disabled").addClass((circularNavigation || floor_index < num_floors - 1) ? "active" : "disabled");

			$building_wrapper.removeClass().addClass('building-wrapper floor_' + floor_id);
			$building_wrapper.find('.floor').removeClass('active').filter('.floor_' + floor_id).addClass('active');
			$building_wrapper.find('.building_floor_link').removeClass('active').filter('.floor_' + floor_id).addClass('active');

			if (typeof track !== 'undefined') {
				track(window.location.pathname + '?floor=' + encodeURIComponent(mred.floors[floor_id].code));
			}
		}

		$.fn.scroll_to_floor_id = function(floor_id) {
			var new_floor_index = $.inArray("" + floor_id, floor_ids);
			if (new_floor_index !== -1) {
				scroll_to_floor(new_floor_index);
			}
		};

		function scroll_to_floor(new_floor_index) {
			if (!$floor_plan_wrapper.visible(false, 'vertical')) {
				$('html, body').stop(true, true).animate({
					// Commenting below to avoid scroll for client
					// scrollTop: $floor_plan_wrapper.offset().top
				}, 'fast');
			}
			$scroll_container.not(':animated').stop(true, true).animate({'top': '-' + (floor_plan_height * new_floor_index) + 'px'}, "fast", "linear", function() {
				floor_index = new_floor_index;
				update_floor_label_and_button_states();
			} );
		}

		floor_scroll_up = function () {
			if (floor_index < num_floors - 1) {
				scroll_to_floor(floor_index + 1);
			}
			else if (circularNavigation) {
				scroll_to_floor(0);
			}
		};

		floor_scroll_down = function () {
			if (floor_index > 0) {
				scroll_to_floor(floor_index - 1);
			}
			else if (circularNavigation) {
				scroll_to_floor(num_floors - 1);
			}
		};

		$scroll_arrow_down.click(function(e){
			e.preventDefault();
			floor_scroll_up();
		});
		$scroll_arrow_up.click(function(e){
			e.preventDefault();
			floor_scroll_down();
		});

		$(".floor-plan-wrapper").touchwipe({
			wipeDown: floor_scroll_down,
			wipeUp: floor_scroll_up,
			min_move_x: 20,
			min_move_y: 20,
			preventDefaultEvents: true
		});

		$('.building-tab').click(function(e) {
			update_floor_label_and_button_states();
		});

		$(window).resize(function() {
			if ($('.plans-by-floor .floor-plan-wrapper').length) {
				floor_plan_height = $('.plans-by-floor .floor-plan-wrapper').outerHeight();
				$scroll_container.css({'top': '-' + (floor_plan_height * floor_index) + 'px'});
				// IE fix.  height: auto isn't sufficient
				$('.plans-by-floor .scroll-container .floor-image-wrapper').height(floor_plan_height + 'px');
			}
		}).trigger('resize');


		$(window).load(function() {
			update_floor_label_and_button_states();
		});
	}

	function mred_resize_filter_arrow() {
		var $chosen = $('.filter-menu > li.chosen');
		if ($chosen.length === 0) {
			return;
		}
		var chosen_width = $chosen.outerWidth(),
		    half_width = Math.floor(chosen_width / 2);

		// Ugly, but only way I can see to resize the arrow.
		$('<style>.filter-menu .chosen:after{border-width: 16px ' + half_width + 'px 0 ' + half_width + 'px; margin-left: -' + half_width + 'px;}</style>').appendTo('head');

	}

	function toggleFilterMenu() {
		if ($('body').hasClass('filter-menu-open')) {
			$('.filter-menu').slideUp(300);
			$('body').removeClass('filter-menu-open').addClass('filter-menu-closed');
		} else {
			$('body').removeClass('filter-menu-closed').addClass('filter-menu-open');
			$('.filter-menu').slideDown(300);
		}
	}

	function mred_activate_filters() {
		var $links = $('.filter-menu a');
		if ($links.length === 0) {
			return;
		}

		$('.filter-responsive-link').click(function() {
			toggleFilterMenu();
		});

		// When a pseudo-filter is clicked, have it simulate clicking the
		// corresponding real filter, so highlighting functionality works
		var $filters = $('.list-filter');
		$filters.click(function(e) {
			e.preventDefault();
			var target = $(this).attr('href');
			$('.filter-menu a[href="' + target + '"]').click();
		});


		$links.click(function(e) {
			var filter_type = $(this).data('filter-type');
			if (typeof filter_type === "undefined") {
				// This is a link, not a filter
				return;
			}
			e.preventDefault();

			var filter_value = $(this).data('filter-value'),
			    $rows = $('.apartment-list-row, .apartment-list-image-wrapper'),
			    to_track = '?' + encodeURIComponent(filter_type) + '=' + encodeURIComponent($(this).text()),
			    tab = $('.view-switcher.active').data('analytics-id'),
			    $selected_rows = $rows;

			if (filter_type == 'all') {
				to_track = '';
				$('.active-filter').hide();
			}
			else {
				$selected_rows = $rows.hide().filter('.' + filter_type + '_' + filter_value);
				$('.filter-value').html($(this).html());
				$('.active-filter').show();
			}

			$selected_rows.removeClass('odd even first-visible');
			$selected_rows.filter(':even').addClass('odd');
			$selected_rows.filter(':odd').addClass('even');
			$selected_rows.filter('.apartment-list-row:first').addClass('first-visible');

			$selected_rows.each(function(n) {
				var row_class = 'column-' + (n % 3 + 1) + '-3 column-' + (n % 4 + 1) + '-4';
				$(this).removeClass('column-1-3 column-2-3 column-3-3 column-1-4 column-2-4 column-3-4 column-4-4').addClass(row_class);
			});
			$selected_rows.slideDown();

			$('.filter-menu > li.chosen').removeClass('chosen');
			var $chosen = $(this).closest('.filter-menu > li');
			$chosen.addClass('chosen');

			mred_resize_filter_arrow();

			if (!superfishFilterActive) {
				// Close the menu if we're on mobile
				toggleFilterMenu();
			}

			if (typeof track !== 'undefined') {
				track('/plans-par-liste/' + tab + to_track);
			}

		});

	}

	function supports_svg() {
		return document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1");
	}

	function mred_use_svg_fallbacks() {
		if (supports_svg()) {
			return;
		}

		$('img[src$=".svg"]').each(function(){
			var $img = $(this);

			if (typeof $img.data('fallback') !== "undefined") {
				$img.attr("src", $img.data('fallback'));
			}
		});
	}

	function add_floor_png_over_functionality() {
		if (isMobile && !showAppartmentDetailPopupOnMobile) {
			return;
		}

		$('.floor_image_map area').each(function() {
			$(this).on('mouseover', function() {
			  var overlay_class = $(this).data('over'),
				  $overlay = $('#overlay_' + overlay_class),
				  $label = $('#label_overlay_' + overlay_class);
			  $overlay.attr('src', $overlay.data('over'));
			  $label.show();
			});

			$(this).on('mouseout', function() {
			  var overlay_class = $(this).data('over'),
				  $overlay = $('#overlay_' + overlay_class),
				  $label = $('#label_overlay_' + overlay_class);

			  $overlay.attr('src', $overlay.data('off'));
			  $label.hide();
			});
		});
	}

	function mred_enable_superfish_for_selector($menus) {
		// Initialize dropdowns
		$menus.superfish({ // Superfish dropdowns
			delay: 100,
			disableHI: true,
			speed: 100,
			animation: {	// fade in and slide down
				opacity: 'show',
				height: 'show'
			}
		});
	}

	function mred_viewport() {
		var e = window, a = 'inner';
		if (!('innerWidth' in window )) {
			a = 'client';
			e = document.documentElement || document.body;
		}
		return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
	}

	function mred_enable_superfish() {
		var viewportsize = mred_viewport();
		superfishFilterActive = viewportsize.width >= minimumSuperfishFilterWidth;
		if (!superfishFilterActive) {
			$('.sf-menu.filter-menu').removeClass('sf-menu');
		}

		mred_enable_superfish_for_selector($('.sf-menu'));

		$(window).resize(function() {
			var viewportsize = mred_viewport();
			var needsSuperfish = viewportsize.width >= minimumSuperfishFilterWidth;
			if ( (needsSuperfish && superfishFilterActive) || (!needsSuperfish && !superfishFilterActive) ) {
				return;
			}
			if (needsSuperfish) {
				$('.filter-menu').addClass('sf-menu').css('display', ''); // remove inline display property, if there is one
				mred_enable_superfish_for_selector($('.sf-menu'));
				mred_resize_filter_arrow();
			}
			else {
				$('.sf-menu.filter-menu').removeClass('sf-menu').superfish('destroy');
			}

			superfishFilterActive = needsSuperfish;
		});
	}

	$(function() {
		mred_track_outbound_clicks();
		mred_add_apartment_list_sort();
		mred_add_apartment_list_switch();
		mred_add_menu_switchers();
		mred_add_floor_navigation();
		mred_activate_filters();
		mred_use_svg_fallbacks();
		add_floor_png_over_functionality();
		mred_enable_superfish();

		$(window).load(function() {
			mred_resize_filter_arrow();
			var hash = location.hash;
			if (hash.length !== 0) {
				var $hash_link =  $('a[href=' + hash + ']').not('.main_menu a[href=' + hash + ']');
				if ($hash_link.length > 0) {
					$hash_link[0].click();
				}
			}
		});
	});

})(jQuery);
